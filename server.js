'use strict';

const express = require("express");
const compression = require("compression");

const _port = 4100;
const _app_folder = 'dist/due-diligence';

const app = express();
const cors = require('cors');

app.use(cors());

app.use(express.static(_app_folder + '/'));

app.listen(process.env.PORT || _port, function () {
    console.log("Node Express server for " + app.name + " listening on http://localhost:" + _port);
});