export enum Status {
    Closed = 'Closed / Converted',
    Active = 'Active',
    Receivership = 'Receivership',
    VoluntaryArrangement = 'Voluntary Arrangement',
    Liquidation = 'Liquidation',
    Transferred = 'Transferred from the UK',
    ActiveStikeOff = 'Active - Proposal to strike off',
    Dissolved = 'Dissolved',
    Administration = 'In administration'
}

export class Company {
    number: number;
    name: string;
    status: Status;
    type: string;
    nature: string;
    incorporated: string;
}
