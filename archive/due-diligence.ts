import {gql} from 'apollo-angular';

export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
};








export type _AddAddressReferenced_InPayload = {
   __typename?: '_AddAddressReferenced_inPayload',
  from?: Maybe<Address>,
  to?: Maybe<Document>,
};

export type _AddAssetReferenced_InPayload = {
   __typename?: '_AddAssetReferenced_inPayload',
  from?: Maybe<Asset>,
  to?: Maybe<Document>,
};

export type _AddAssetRegistered_AtPayload = {
   __typename?: '_AddAssetRegistered_atPayload',
  from?: Maybe<Asset>,
  to?: Maybe<Address>,
};

export type _AddCompanyOwner_OfPayload = {
   __typename?: '_AddCompanyOwner_ofPayload',
  from?: Maybe<Company>,
  to?: Maybe<Asset>,
};

export type _AddCompanyReferenced_InPayload = {
   __typename?: '_AddCompanyReferenced_inPayload',
  from?: Maybe<Company>,
  to?: Maybe<Document>,
};

export type _AddCompanyRegistered_AtPayload = {
   __typename?: '_AddCompanyRegistered_atPayload',
  from?: Maybe<Company>,
  to?: Maybe<Address>,
};

export type _AddCompanyShareholder_OfPayload = {
   __typename?: '_AddCompanyShareholder_ofPayload',
  from?: Maybe<Company>,
  to?: Maybe<Company>,
};

export type _AddDocumentAuthorPayload = {
   __typename?: '_AddDocumentAuthorPayload',
  from?: Maybe<Document>,
  to?: Maybe<Person>,
};

export type _AddEventReferenced_InPayload = {
   __typename?: '_AddEventReferenced_inPayload',
  from?: Maybe<Event>,
  to?: Maybe<Document>,
};

export type _AddPersonOwner_OfPayload = {
   __typename?: '_AddPersonOwner_ofPayload',
  from?: Maybe<Person>,
  to?: Maybe<Asset>,
};

export type _AddPersonReferenced_InPayload = {
   __typename?: '_AddPersonReferenced_inPayload',
  from?: Maybe<Person>,
  to?: Maybe<Document>,
};

export type _AddPersonShareholder_OfPayload = {
   __typename?: '_AddPersonShareholder_ofPayload',
  from?: Maybe<Person>,
  to?: Maybe<Company>,
};

export type _AddressFilter = {
  AND?: Maybe<Array<_AddressFilter>>,
  OR?: Maybe<Array<_AddressFilter>>,
  address?: Maybe<Scalars['String']>,
  address_not?: Maybe<Scalars['String']>,
  address_in?: Maybe<Array<Scalars['String']>>,
  address_not_in?: Maybe<Array<Scalars['String']>>,
  address_contains?: Maybe<Scalars['String']>,
  address_not_contains?: Maybe<Scalars['String']>,
  address_starts_with?: Maybe<Scalars['String']>,
  address_not_starts_with?: Maybe<Scalars['String']>,
  address_ends_with?: Maybe<Scalars['String']>,
  address_not_ends_with?: Maybe<Scalars['String']>,
  city?: Maybe<Scalars['String']>,
  city_not?: Maybe<Scalars['String']>,
  city_in?: Maybe<Array<Scalars['String']>>,
  city_not_in?: Maybe<Array<Scalars['String']>>,
  city_contains?: Maybe<Scalars['String']>,
  city_not_contains?: Maybe<Scalars['String']>,
  city_starts_with?: Maybe<Scalars['String']>,
  city_not_starts_with?: Maybe<Scalars['String']>,
  city_ends_with?: Maybe<Scalars['String']>,
  city_not_ends_with?: Maybe<Scalars['String']>,
  State?: Maybe<Scalars['String']>,
  State_not?: Maybe<Scalars['String']>,
  State_in?: Maybe<Array<Scalars['String']>>,
  State_not_in?: Maybe<Array<Scalars['String']>>,
  State_contains?: Maybe<Scalars['String']>,
  State_not_contains?: Maybe<Scalars['String']>,
  State_starts_with?: Maybe<Scalars['String']>,
  State_not_starts_with?: Maybe<Scalars['String']>,
  State_ends_with?: Maybe<Scalars['String']>,
  State_not_ends_with?: Maybe<Scalars['String']>,
  country?: Maybe<Scalars['String']>,
  country_not?: Maybe<Scalars['String']>,
  country_in?: Maybe<Array<Scalars['String']>>,
  country_not_in?: Maybe<Array<Scalars['String']>>,
  country_contains?: Maybe<Scalars['String']>,
  country_not_contains?: Maybe<Scalars['String']>,
  country_starts_with?: Maybe<Scalars['String']>,
  country_not_starts_with?: Maybe<Scalars['String']>,
  country_ends_with?: Maybe<Scalars['String']>,
  country_not_ends_with?: Maybe<Scalars['String']>,
  referenced_in?: Maybe<_DocumentFilter>,
  referenced_in_not?: Maybe<_DocumentFilter>,
  referenced_in_in?: Maybe<Array<_DocumentFilter>>,
  referenced_in_not_in?: Maybe<Array<_DocumentFilter>>,
  referenced_in_some?: Maybe<_DocumentFilter>,
  referenced_in_none?: Maybe<_DocumentFilter>,
  referenced_in_single?: Maybe<_DocumentFilter>,
  referenced_in_every?: Maybe<_DocumentFilter>,
};

export type _AddressInput = {
  address: Scalars['String'],
};

export enum _AddressOrdering {
  AddressAsc = 'address_asc',
  AddressDesc = 'address_desc',
  CityAsc = 'city_asc',
  CityDesc = 'city_desc',
  StateAsc = 'State_asc',
  StateDesc = 'State_desc',
  CountryAsc = 'country_asc',
  CountryDesc = 'country_desc',
  IdAsc = '_id_asc',
  IdDesc = '_id_desc'
}

export type _AssetFilter = {
  AND?: Maybe<Array<_AssetFilter>>,
  OR?: Maybe<Array<_AssetFilter>>,
  name?: Maybe<Scalars['String']>,
  name_not?: Maybe<Scalars['String']>,
  name_in?: Maybe<Array<Scalars['String']>>,
  name_not_in?: Maybe<Array<Scalars['String']>>,
  name_contains?: Maybe<Scalars['String']>,
  name_not_contains?: Maybe<Scalars['String']>,
  name_starts_with?: Maybe<Scalars['String']>,
  name_not_starts_with?: Maybe<Scalars['String']>,
  name_ends_with?: Maybe<Scalars['String']>,
  name_not_ends_with?: Maybe<Scalars['String']>,
  value?: Maybe<Scalars['String']>,
  value_not?: Maybe<Scalars['String']>,
  value_in?: Maybe<Array<Scalars['String']>>,
  value_not_in?: Maybe<Array<Scalars['String']>>,
  value_contains?: Maybe<Scalars['String']>,
  value_not_contains?: Maybe<Scalars['String']>,
  value_starts_with?: Maybe<Scalars['String']>,
  value_not_starts_with?: Maybe<Scalars['String']>,
  value_ends_with?: Maybe<Scalars['String']>,
  value_not_ends_with?: Maybe<Scalars['String']>,
  type?: Maybe<Scalars['String']>,
  type_not?: Maybe<Scalars['String']>,
  type_in?: Maybe<Array<Scalars['String']>>,
  type_not_in?: Maybe<Array<Scalars['String']>>,
  type_contains?: Maybe<Scalars['String']>,
  type_not_contains?: Maybe<Scalars['String']>,
  type_starts_with?: Maybe<Scalars['String']>,
  type_not_starts_with?: Maybe<Scalars['String']>,
  type_ends_with?: Maybe<Scalars['String']>,
  type_not_ends_with?: Maybe<Scalars['String']>,
  description?: Maybe<Scalars['String']>,
  description_not?: Maybe<Scalars['String']>,
  description_in?: Maybe<Array<Scalars['String']>>,
  description_not_in?: Maybe<Array<Scalars['String']>>,
  description_contains?: Maybe<Scalars['String']>,
  description_not_contains?: Maybe<Scalars['String']>,
  description_starts_with?: Maybe<Scalars['String']>,
  description_not_starts_with?: Maybe<Scalars['String']>,
  description_ends_with?: Maybe<Scalars['String']>,
  description_not_ends_with?: Maybe<Scalars['String']>,
  registered_at?: Maybe<_AddressFilter>,
  registered_at_not?: Maybe<_AddressFilter>,
  registered_at_in?: Maybe<Array<_AddressFilter>>,
  registered_at_not_in?: Maybe<Array<_AddressFilter>>,
  registered_at_some?: Maybe<_AddressFilter>,
  registered_at_none?: Maybe<_AddressFilter>,
  registered_at_single?: Maybe<_AddressFilter>,
  registered_at_every?: Maybe<_AddressFilter>,
  referenced_in?: Maybe<_DocumentFilter>,
  referenced_in_not?: Maybe<_DocumentFilter>,
  referenced_in_in?: Maybe<Array<_DocumentFilter>>,
  referenced_in_not_in?: Maybe<Array<_DocumentFilter>>,
  referenced_in_some?: Maybe<_DocumentFilter>,
  referenced_in_none?: Maybe<_DocumentFilter>,
  referenced_in_single?: Maybe<_DocumentFilter>,
  referenced_in_every?: Maybe<_DocumentFilter>,
};

export type _AssetInput = {
  name: Scalars['String'],
};

export enum _AssetOrdering {
  NameAsc = 'name_asc',
  NameDesc = 'name_desc',
  ValueAsc = 'value_asc',
  ValueDesc = 'value_desc',
  TypeAsc = 'type_asc',
  TypeDesc = 'type_desc',
  DescriptionAsc = 'description_asc',
  DescriptionDesc = 'description_desc',
  IdAsc = '_id_asc',
  IdDesc = '_id_desc'
}

export type _CompanyFilter = {
  AND?: Maybe<Array<_CompanyFilter>>,
  OR?: Maybe<Array<_CompanyFilter>>,
  name?: Maybe<Scalars['String']>,
  name_not?: Maybe<Scalars['String']>,
  name_in?: Maybe<Array<Scalars['String']>>,
  name_not_in?: Maybe<Array<Scalars['String']>>,
  name_contains?: Maybe<Scalars['String']>,
  name_not_contains?: Maybe<Scalars['String']>,
  name_starts_with?: Maybe<Scalars['String']>,
  name_not_starts_with?: Maybe<Scalars['String']>,
  name_ends_with?: Maybe<Scalars['String']>,
  name_not_ends_with?: Maybe<Scalars['String']>,
  shareholder_of?: Maybe<_CompanyFilter>,
  shareholder_of_not?: Maybe<_CompanyFilter>,
  shareholder_of_in?: Maybe<Array<_CompanyFilter>>,
  shareholder_of_not_in?: Maybe<Array<_CompanyFilter>>,
  shareholder_of_some?: Maybe<_CompanyFilter>,
  shareholder_of_none?: Maybe<_CompanyFilter>,
  shareholder_of_single?: Maybe<_CompanyFilter>,
  shareholder_of_every?: Maybe<_CompanyFilter>,
  owner_of?: Maybe<_AssetFilter>,
  owner_of_not?: Maybe<_AssetFilter>,
  owner_of_in?: Maybe<Array<_AssetFilter>>,
  owner_of_not_in?: Maybe<Array<_AssetFilter>>,
  owner_of_some?: Maybe<_AssetFilter>,
  owner_of_none?: Maybe<_AssetFilter>,
  owner_of_single?: Maybe<_AssetFilter>,
  owner_of_every?: Maybe<_AssetFilter>,
  registered_at?: Maybe<_AddressFilter>,
  registered_at_not?: Maybe<_AddressFilter>,
  registered_at_in?: Maybe<Array<_AddressFilter>>,
  registered_at_not_in?: Maybe<Array<_AddressFilter>>,
  registered_at_some?: Maybe<_AddressFilter>,
  registered_at_none?: Maybe<_AddressFilter>,
  registered_at_single?: Maybe<_AddressFilter>,
  registered_at_every?: Maybe<_AddressFilter>,
  referenced_in?: Maybe<_DocumentFilter>,
  referenced_in_not?: Maybe<_DocumentFilter>,
  referenced_in_in?: Maybe<Array<_DocumentFilter>>,
  referenced_in_not_in?: Maybe<Array<_DocumentFilter>>,
  referenced_in_some?: Maybe<_DocumentFilter>,
  referenced_in_none?: Maybe<_DocumentFilter>,
  referenced_in_single?: Maybe<_DocumentFilter>,
  referenced_in_every?: Maybe<_DocumentFilter>,
};

export type _CompanyInput = {
  name: Scalars['String'],
};

export enum _CompanyOrdering {
  NameAsc = 'name_asc',
  NameDesc = 'name_desc',
  IdAsc = '_id_asc',
  IdDesc = '_id_desc'
}

export type _DocumentFilter = {
  AND?: Maybe<Array<_DocumentFilter>>,
  OR?: Maybe<Array<_DocumentFilter>>,
  title?: Maybe<Scalars['String']>,
  title_not?: Maybe<Scalars['String']>,
  title_in?: Maybe<Array<Scalars['String']>>,
  title_not_in?: Maybe<Array<Scalars['String']>>,
  title_contains?: Maybe<Scalars['String']>,
  title_not_contains?: Maybe<Scalars['String']>,
  title_starts_with?: Maybe<Scalars['String']>,
  title_not_starts_with?: Maybe<Scalars['String']>,
  title_ends_with?: Maybe<Scalars['String']>,
  title_not_ends_with?: Maybe<Scalars['String']>,
  url?: Maybe<Scalars['String']>,
  url_not?: Maybe<Scalars['String']>,
  url_in?: Maybe<Array<Scalars['String']>>,
  url_not_in?: Maybe<Array<Scalars['String']>>,
  url_contains?: Maybe<Scalars['String']>,
  url_not_contains?: Maybe<Scalars['String']>,
  url_starts_with?: Maybe<Scalars['String']>,
  url_not_starts_with?: Maybe<Scalars['String']>,
  url_ends_with?: Maybe<Scalars['String']>,
  url_not_ends_with?: Maybe<Scalars['String']>,
  publication_date?: Maybe<Scalars['String']>,
  publication_date_not?: Maybe<Scalars['String']>,
  publication_date_in?: Maybe<Array<Scalars['String']>>,
  publication_date_not_in?: Maybe<Array<Scalars['String']>>,
  publication_date_contains?: Maybe<Scalars['String']>,
  publication_date_not_contains?: Maybe<Scalars['String']>,
  publication_date_starts_with?: Maybe<Scalars['String']>,
  publication_date_not_starts_with?: Maybe<Scalars['String']>,
  publication_date_ends_with?: Maybe<Scalars['String']>,
  publication_date_not_ends_with?: Maybe<Scalars['String']>,
  author?: Maybe<_PersonFilter>,
  author_not?: Maybe<_PersonFilter>,
  author_in?: Maybe<Array<_PersonFilter>>,
  author_not_in?: Maybe<Array<_PersonFilter>>,
  author_some?: Maybe<_PersonFilter>,
  author_none?: Maybe<_PersonFilter>,
  author_single?: Maybe<_PersonFilter>,
  author_every?: Maybe<_PersonFilter>,
};

export type _DocumentInput = {
  title: Scalars['String'],
};

export enum _DocumentOrdering {
  TitleAsc = 'title_asc',
  TitleDesc = 'title_desc',
  UrlAsc = 'url_asc',
  UrlDesc = 'url_desc',
  PublicationDateAsc = 'publication_date_asc',
  PublicationDateDesc = 'publication_date_desc',
  IdAsc = '_id_asc',
  IdDesc = '_id_desc'
}

export type _EventFilter = {
  AND?: Maybe<Array<_EventFilter>>,
  OR?: Maybe<Array<_EventFilter>>,
  date?: Maybe<Scalars['String']>,
  date_not?: Maybe<Scalars['String']>,
  date_in?: Maybe<Array<Scalars['String']>>,
  date_not_in?: Maybe<Array<Scalars['String']>>,
  date_contains?: Maybe<Scalars['String']>,
  date_not_contains?: Maybe<Scalars['String']>,
  date_starts_with?: Maybe<Scalars['String']>,
  date_not_starts_with?: Maybe<Scalars['String']>,
  date_ends_with?: Maybe<Scalars['String']>,
  date_not_ends_with?: Maybe<Scalars['String']>,
  Description?: Maybe<Scalars['String']>,
  Description_not?: Maybe<Scalars['String']>,
  Description_in?: Maybe<Array<Scalars['String']>>,
  Description_not_in?: Maybe<Array<Scalars['String']>>,
  Description_contains?: Maybe<Scalars['String']>,
  Description_not_contains?: Maybe<Scalars['String']>,
  Description_starts_with?: Maybe<Scalars['String']>,
  Description_not_starts_with?: Maybe<Scalars['String']>,
  Description_ends_with?: Maybe<Scalars['String']>,
  Description_not_ends_with?: Maybe<Scalars['String']>,
  referenced_in?: Maybe<_DocumentFilter>,
  referenced_in_not?: Maybe<_DocumentFilter>,
  referenced_in_in?: Maybe<Array<_DocumentFilter>>,
  referenced_in_not_in?: Maybe<Array<_DocumentFilter>>,
  referenced_in_some?: Maybe<_DocumentFilter>,
  referenced_in_none?: Maybe<_DocumentFilter>,
  referenced_in_single?: Maybe<_DocumentFilter>,
  referenced_in_every?: Maybe<_DocumentFilter>,
};

export type _EventInput = {
  date: Scalars['String'],
};

export enum _EventOrdering {
  DateAsc = 'date_asc',
  DateDesc = 'date_desc',
  DescriptionAsc = 'Description_asc',
  DescriptionDesc = 'Description_desc',
  IdAsc = '_id_asc',
  IdDesc = '_id_desc'
}

export type _Neo4jDate = {
   __typename?: '_Neo4jDate',
  year?: Maybe<Scalars['Int']>,
  month?: Maybe<Scalars['Int']>,
  day?: Maybe<Scalars['Int']>,
  formatted?: Maybe<Scalars['String']>,
};

export type _Neo4jDateInput = {
  year?: Maybe<Scalars['Int']>,
  month?: Maybe<Scalars['Int']>,
  day?: Maybe<Scalars['Int']>,
  formatted?: Maybe<Scalars['String']>,
};

export type _Neo4jDateTime = {
   __typename?: '_Neo4jDateTime',
  year?: Maybe<Scalars['Int']>,
  month?: Maybe<Scalars['Int']>,
  day?: Maybe<Scalars['Int']>,
  hour?: Maybe<Scalars['Int']>,
  minute?: Maybe<Scalars['Int']>,
  second?: Maybe<Scalars['Int']>,
  millisecond?: Maybe<Scalars['Int']>,
  microsecond?: Maybe<Scalars['Int']>,
  nanosecond?: Maybe<Scalars['Int']>,
  timezone?: Maybe<Scalars['String']>,
  formatted?: Maybe<Scalars['String']>,
};

export type _Neo4jDateTimeInput = {
  year?: Maybe<Scalars['Int']>,
  month?: Maybe<Scalars['Int']>,
  day?: Maybe<Scalars['Int']>,
  hour?: Maybe<Scalars['Int']>,
  minute?: Maybe<Scalars['Int']>,
  second?: Maybe<Scalars['Int']>,
  millisecond?: Maybe<Scalars['Int']>,
  microsecond?: Maybe<Scalars['Int']>,
  nanosecond?: Maybe<Scalars['Int']>,
  timezone?: Maybe<Scalars['String']>,
  formatted?: Maybe<Scalars['String']>,
};

export type _Neo4jLocalDateTime = {
   __typename?: '_Neo4jLocalDateTime',
  year?: Maybe<Scalars['Int']>,
  month?: Maybe<Scalars['Int']>,
  day?: Maybe<Scalars['Int']>,
  hour?: Maybe<Scalars['Int']>,
  minute?: Maybe<Scalars['Int']>,
  second?: Maybe<Scalars['Int']>,
  millisecond?: Maybe<Scalars['Int']>,
  microsecond?: Maybe<Scalars['Int']>,
  nanosecond?: Maybe<Scalars['Int']>,
  formatted?: Maybe<Scalars['String']>,
};

export type _Neo4jLocalDateTimeInput = {
  year?: Maybe<Scalars['Int']>,
  month?: Maybe<Scalars['Int']>,
  day?: Maybe<Scalars['Int']>,
  hour?: Maybe<Scalars['Int']>,
  minute?: Maybe<Scalars['Int']>,
  second?: Maybe<Scalars['Int']>,
  millisecond?: Maybe<Scalars['Int']>,
  microsecond?: Maybe<Scalars['Int']>,
  nanosecond?: Maybe<Scalars['Int']>,
  formatted?: Maybe<Scalars['String']>,
};

export type _Neo4jLocalTime = {
   __typename?: '_Neo4jLocalTime',
  hour?: Maybe<Scalars['Int']>,
  minute?: Maybe<Scalars['Int']>,
  second?: Maybe<Scalars['Int']>,
  millisecond?: Maybe<Scalars['Int']>,
  microsecond?: Maybe<Scalars['Int']>,
  nanosecond?: Maybe<Scalars['Int']>,
  formatted?: Maybe<Scalars['String']>,
};

export type _Neo4jLocalTimeInput = {
  hour?: Maybe<Scalars['Int']>,
  minute?: Maybe<Scalars['Int']>,
  second?: Maybe<Scalars['Int']>,
  millisecond?: Maybe<Scalars['Int']>,
  microsecond?: Maybe<Scalars['Int']>,
  nanosecond?: Maybe<Scalars['Int']>,
  formatted?: Maybe<Scalars['String']>,
};

export type _Neo4jTime = {
   __typename?: '_Neo4jTime',
  hour?: Maybe<Scalars['Int']>,
  minute?: Maybe<Scalars['Int']>,
  second?: Maybe<Scalars['Int']>,
  millisecond?: Maybe<Scalars['Int']>,
  microsecond?: Maybe<Scalars['Int']>,
  nanosecond?: Maybe<Scalars['Int']>,
  timezone?: Maybe<Scalars['String']>,
  formatted?: Maybe<Scalars['String']>,
};

export type _Neo4jTimeInput = {
  hour?: Maybe<Scalars['Int']>,
  minute?: Maybe<Scalars['Int']>,
  second?: Maybe<Scalars['Int']>,
  millisecond?: Maybe<Scalars['Int']>,
  microsecond?: Maybe<Scalars['Int']>,
  nanosecond?: Maybe<Scalars['Int']>,
  timezone?: Maybe<Scalars['String']>,
  formatted?: Maybe<Scalars['String']>,
};

export type _PersonFilter = {
  AND?: Maybe<Array<_PersonFilter>>,
  OR?: Maybe<Array<_PersonFilter>>,
  name?: Maybe<Scalars['String']>,
  name_not?: Maybe<Scalars['String']>,
  name_in?: Maybe<Array<Scalars['String']>>,
  name_not_in?: Maybe<Array<Scalars['String']>>,
  name_contains?: Maybe<Scalars['String']>,
  name_not_contains?: Maybe<Scalars['String']>,
  name_starts_with?: Maybe<Scalars['String']>,
  name_not_starts_with?: Maybe<Scalars['String']>,
  name_ends_with?: Maybe<Scalars['String']>,
  name_not_ends_with?: Maybe<Scalars['String']>,
  nationality?: Maybe<Scalars['String']>,
  nationality_not?: Maybe<Scalars['String']>,
  nationality_in?: Maybe<Array<Scalars['String']>>,
  nationality_not_in?: Maybe<Array<Scalars['String']>>,
  nationality_contains?: Maybe<Scalars['String']>,
  nationality_not_contains?: Maybe<Scalars['String']>,
  nationality_starts_with?: Maybe<Scalars['String']>,
  nationality_not_starts_with?: Maybe<Scalars['String']>,
  nationality_ends_with?: Maybe<Scalars['String']>,
  nationality_not_ends_with?: Maybe<Scalars['String']>,
  country_of_residence?: Maybe<Scalars['String']>,
  country_of_residence_not?: Maybe<Scalars['String']>,
  country_of_residence_in?: Maybe<Array<Scalars['String']>>,
  country_of_residence_not_in?: Maybe<Array<Scalars['String']>>,
  country_of_residence_contains?: Maybe<Scalars['String']>,
  country_of_residence_not_contains?: Maybe<Scalars['String']>,
  country_of_residence_starts_with?: Maybe<Scalars['String']>,
  country_of_residence_not_starts_with?: Maybe<Scalars['String']>,
  country_of_residence_ends_with?: Maybe<Scalars['String']>,
  country_of_residence_not_ends_with?: Maybe<Scalars['String']>,
  dob?: Maybe<Scalars['String']>,
  dob_not?: Maybe<Scalars['String']>,
  dob_in?: Maybe<Array<Scalars['String']>>,
  dob_not_in?: Maybe<Array<Scalars['String']>>,
  dob_contains?: Maybe<Scalars['String']>,
  dob_not_contains?: Maybe<Scalars['String']>,
  dob_starts_with?: Maybe<Scalars['String']>,
  dob_not_starts_with?: Maybe<Scalars['String']>,
  dob_ends_with?: Maybe<Scalars['String']>,
  dob_not_ends_with?: Maybe<Scalars['String']>,
  shareholder_of?: Maybe<_CompanyFilter>,
  shareholder_of_not?: Maybe<_CompanyFilter>,
  shareholder_of_in?: Maybe<Array<_CompanyFilter>>,
  shareholder_of_not_in?: Maybe<Array<_CompanyFilter>>,
  shareholder_of_some?: Maybe<_CompanyFilter>,
  shareholder_of_none?: Maybe<_CompanyFilter>,
  shareholder_of_single?: Maybe<_CompanyFilter>,
  shareholder_of_every?: Maybe<_CompanyFilter>,
  owner_of?: Maybe<_AssetFilter>,
  owner_of_not?: Maybe<_AssetFilter>,
  owner_of_in?: Maybe<Array<_AssetFilter>>,
  owner_of_not_in?: Maybe<Array<_AssetFilter>>,
  owner_of_some?: Maybe<_AssetFilter>,
  owner_of_none?: Maybe<_AssetFilter>,
  owner_of_single?: Maybe<_AssetFilter>,
  owner_of_every?: Maybe<_AssetFilter>,
  referenced_in?: Maybe<_DocumentFilter>,
  referenced_in_not?: Maybe<_DocumentFilter>,
  referenced_in_in?: Maybe<Array<_DocumentFilter>>,
  referenced_in_not_in?: Maybe<Array<_DocumentFilter>>,
  referenced_in_some?: Maybe<_DocumentFilter>,
  referenced_in_none?: Maybe<_DocumentFilter>,
  referenced_in_single?: Maybe<_DocumentFilter>,
  referenced_in_every?: Maybe<_DocumentFilter>,
};

export type _PersonInput = {
  name: Scalars['String'],
};

export enum _PersonOrdering {
  NameAsc = 'name_asc',
  NameDesc = 'name_desc',
  NationalityAsc = 'nationality_asc',
  NationalityDesc = 'nationality_desc',
  CountryOfResidenceAsc = 'country_of_residence_asc',
  CountryOfResidenceDesc = 'country_of_residence_desc',
  DobAsc = 'dob_asc',
  DobDesc = 'dob_desc',
  IdAsc = '_id_asc',
  IdDesc = '_id_desc'
}

export enum _RelationDirections {
  In = 'IN',
  Out = 'OUT'
}

export type _RemoveAddressReferenced_InPayload = {
   __typename?: '_RemoveAddressReferenced_inPayload',
  from?: Maybe<Address>,
  to?: Maybe<Document>,
};

export type _RemoveAssetReferenced_InPayload = {
   __typename?: '_RemoveAssetReferenced_inPayload',
  from?: Maybe<Asset>,
  to?: Maybe<Document>,
};

export type _RemoveAssetRegistered_AtPayload = {
   __typename?: '_RemoveAssetRegistered_atPayload',
  from?: Maybe<Asset>,
  to?: Maybe<Address>,
};

export type _RemoveCompanyOwner_OfPayload = {
   __typename?: '_RemoveCompanyOwner_ofPayload',
  from?: Maybe<Company>,
  to?: Maybe<Asset>,
};

export type _RemoveCompanyReferenced_InPayload = {
   __typename?: '_RemoveCompanyReferenced_inPayload',
  from?: Maybe<Company>,
  to?: Maybe<Document>,
};

export type _RemoveCompanyRegistered_AtPayload = {
   __typename?: '_RemoveCompanyRegistered_atPayload',
  from?: Maybe<Company>,
  to?: Maybe<Address>,
};

export type _RemoveCompanyShareholder_OfPayload = {
   __typename?: '_RemoveCompanyShareholder_ofPayload',
  from?: Maybe<Company>,
  to?: Maybe<Company>,
};

export type _RemoveDocumentAuthorPayload = {
   __typename?: '_RemoveDocumentAuthorPayload',
  from?: Maybe<Document>,
  to?: Maybe<Person>,
};

export type _RemoveEventReferenced_InPayload = {
   __typename?: '_RemoveEventReferenced_inPayload',
  from?: Maybe<Event>,
  to?: Maybe<Document>,
};

export type _RemovePersonOwner_OfPayload = {
   __typename?: '_RemovePersonOwner_ofPayload',
  from?: Maybe<Person>,
  to?: Maybe<Asset>,
};

export type _RemovePersonReferenced_InPayload = {
   __typename?: '_RemovePersonReferenced_inPayload',
  from?: Maybe<Person>,
  to?: Maybe<Document>,
};

export type _RemovePersonShareholder_OfPayload = {
   __typename?: '_RemovePersonShareholder_ofPayload',
  from?: Maybe<Person>,
  to?: Maybe<Company>,
};

export type Address = {
   __typename?: 'Address',
  address?: Maybe<Scalars['String']>,
  city?: Maybe<Scalars['String']>,
  State?: Maybe<Scalars['String']>,
  country?: Maybe<Scalars['String']>,
  referenced_in?: Maybe<Array<Maybe<Document>>>,
  _id?: Maybe<Scalars['String']>,
};


export type AddressReferenced_InArgs = {
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_DocumentOrdering>>>,
  filter?: Maybe<_DocumentFilter>
};

export type Asset = {
   __typename?: 'Asset',
  name?: Maybe<Scalars['String']>,
  value?: Maybe<Scalars['String']>,
  type?: Maybe<Scalars['String']>,
  description?: Maybe<Scalars['String']>,
  registered_at?: Maybe<Array<Maybe<Address>>>,
  referenced_in?: Maybe<Array<Maybe<Document>>>,
  _id?: Maybe<Scalars['String']>,
};


export type AssetRegistered_AtArgs = {
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_AddressOrdering>>>,
  filter?: Maybe<_AddressFilter>
};


export type AssetReferenced_InArgs = {
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_DocumentOrdering>>>,
  filter?: Maybe<_DocumentFilter>
};

export type Company = {
   __typename?: 'Company',
  name?: Maybe<Scalars['String']>,
  shareholder_of?: Maybe<Array<Maybe<Company>>>,
  owner_of?: Maybe<Array<Maybe<Asset>>>,
  registered_at?: Maybe<Array<Maybe<Address>>>,
  referenced_in?: Maybe<Array<Maybe<Document>>>,
  _id?: Maybe<Scalars['String']>,
};


export type CompanyShareholder_OfArgs = {
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_CompanyOrdering>>>,
  filter?: Maybe<_CompanyFilter>
};


export type CompanyOwner_OfArgs = {
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_AssetOrdering>>>,
  filter?: Maybe<_AssetFilter>
};


export type CompanyRegistered_AtArgs = {
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_AddressOrdering>>>,
  filter?: Maybe<_AddressFilter>
};


export type CompanyReferenced_InArgs = {
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_DocumentOrdering>>>,
  filter?: Maybe<_DocumentFilter>
};

export type Document = {
   __typename?: 'Document',
  title?: Maybe<Scalars['String']>,
  url?: Maybe<Scalars['String']>,
  publication_date?: Maybe<Scalars['String']>,
  author?: Maybe<Array<Maybe<Person>>>,
  _id?: Maybe<Scalars['String']>,
};


export type DocumentAuthorArgs = {
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_PersonOrdering>>>,
  filter?: Maybe<_PersonFilter>
};

export type Event = {
   __typename?: 'Event',
  date?: Maybe<Scalars['String']>,
  Description?: Maybe<Scalars['String']>,
  referenced_in?: Maybe<Array<Maybe<Document>>>,
  _id?: Maybe<Scalars['String']>,
};


export type EventReferenced_InArgs = {
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_DocumentOrdering>>>,
  filter?: Maybe<_DocumentFilter>
};

export type Mutation = {
   __typename?: 'Mutation',
  AddDocumentAuthor?: Maybe<_AddDocumentAuthorPayload>,
  RemoveDocumentAuthor?: Maybe<_RemoveDocumentAuthorPayload>,
  CreateDocument?: Maybe<Document>,
  UpdateDocument?: Maybe<Document>,
  DeleteDocument?: Maybe<Document>,
  AddPersonShareholder_of?: Maybe<_AddPersonShareholder_OfPayload>,
  RemovePersonShareholder_of?: Maybe<_RemovePersonShareholder_OfPayload>,
  AddPersonOwner_of?: Maybe<_AddPersonOwner_OfPayload>,
  RemovePersonOwner_of?: Maybe<_RemovePersonOwner_OfPayload>,
  AddPersonReferenced_in?: Maybe<_AddPersonReferenced_InPayload>,
  RemovePersonReferenced_in?: Maybe<_RemovePersonReferenced_InPayload>,
  CreatePerson?: Maybe<Person>,
  UpdatePerson?: Maybe<Person>,
  DeletePerson?: Maybe<Person>,
  AddCompanyShareholder_of?: Maybe<_AddCompanyShareholder_OfPayload>,
  RemoveCompanyShareholder_of?: Maybe<_RemoveCompanyShareholder_OfPayload>,
  AddCompanyOwner_of?: Maybe<_AddCompanyOwner_OfPayload>,
  RemoveCompanyOwner_of?: Maybe<_RemoveCompanyOwner_OfPayload>,
  AddCompanyRegistered_at?: Maybe<_AddCompanyRegistered_AtPayload>,
  RemoveCompanyRegistered_at?: Maybe<_RemoveCompanyRegistered_AtPayload>,
  AddCompanyReferenced_in?: Maybe<_AddCompanyReferenced_InPayload>,
  RemoveCompanyReferenced_in?: Maybe<_RemoveCompanyReferenced_InPayload>,
  CreateCompany?: Maybe<Company>,
  DeleteCompany?: Maybe<Company>,
  AddAssetRegistered_at?: Maybe<_AddAssetRegistered_AtPayload>,
  RemoveAssetRegistered_at?: Maybe<_RemoveAssetRegistered_AtPayload>,
  AddAssetReferenced_in?: Maybe<_AddAssetReferenced_InPayload>,
  RemoveAssetReferenced_in?: Maybe<_RemoveAssetReferenced_InPayload>,
  CreateAsset?: Maybe<Asset>,
  UpdateAsset?: Maybe<Asset>,
  DeleteAsset?: Maybe<Asset>,
  AddAddressReferenced_in?: Maybe<_AddAddressReferenced_InPayload>,
  RemoveAddressReferenced_in?: Maybe<_RemoveAddressReferenced_InPayload>,
  CreateAddress?: Maybe<Address>,
  UpdateAddress?: Maybe<Address>,
  DeleteAddress?: Maybe<Address>,
  AddEventReferenced_in?: Maybe<_AddEventReferenced_InPayload>,
  RemoveEventReferenced_in?: Maybe<_RemoveEventReferenced_InPayload>,
  CreateEvent?: Maybe<Event>,
  UpdateEvent?: Maybe<Event>,
  DeleteEvent?: Maybe<Event>,
};


export type MutationAddDocumentAuthorArgs = {
  from: _DocumentInput,
  to: _PersonInput
};


export type MutationRemoveDocumentAuthorArgs = {
  from: _DocumentInput,
  to: _PersonInput
};


export type MutationCreateDocumentArgs = {
  title?: Maybe<Scalars['String']>,
  url?: Maybe<Scalars['String']>,
  publication_date?: Maybe<Scalars['String']>
};


export type MutationUpdateDocumentArgs = {
  title: Scalars['String'],
  url?: Maybe<Scalars['String']>,
  publication_date?: Maybe<Scalars['String']>
};


export type MutationDeleteDocumentArgs = {
  title: Scalars['String']
};


export type MutationAddPersonShareholder_OfArgs = {
  from: _PersonInput,
  to: _CompanyInput
};


export type MutationRemovePersonShareholder_OfArgs = {
  from: _PersonInput,
  to: _CompanyInput
};


export type MutationAddPersonOwner_OfArgs = {
  from: _PersonInput,
  to: _AssetInput
};


export type MutationRemovePersonOwner_OfArgs = {
  from: _PersonInput,
  to: _AssetInput
};


export type MutationAddPersonReferenced_InArgs = {
  from: _PersonInput,
  to: _DocumentInput
};


export type MutationRemovePersonReferenced_InArgs = {
  from: _PersonInput,
  to: _DocumentInput
};


export type MutationCreatePersonArgs = {
  name?: Maybe<Scalars['String']>,
  nationality?: Maybe<Scalars['String']>,
  country_of_residence?: Maybe<Scalars['String']>,
  dob?: Maybe<Scalars['String']>
};


export type MutationUpdatePersonArgs = {
  name: Scalars['String'],
  nationality?: Maybe<Scalars['String']>,
  country_of_residence?: Maybe<Scalars['String']>,
  dob?: Maybe<Scalars['String']>
};


export type MutationDeletePersonArgs = {
  name: Scalars['String']
};


export type MutationAddCompanyShareholder_OfArgs = {
  from: _CompanyInput,
  to: _CompanyInput
};


export type MutationRemoveCompanyShareholder_OfArgs = {
  from: _CompanyInput,
  to: _CompanyInput
};


export type MutationAddCompanyOwner_OfArgs = {
  from: _CompanyInput,
  to: _AssetInput
};


export type MutationRemoveCompanyOwner_OfArgs = {
  from: _CompanyInput,
  to: _AssetInput
};


export type MutationAddCompanyRegistered_AtArgs = {
  from: _CompanyInput,
  to: _AddressInput
};


export type MutationRemoveCompanyRegistered_AtArgs = {
  from: _CompanyInput,
  to: _AddressInput
};


export type MutationAddCompanyReferenced_InArgs = {
  from: _CompanyInput,
  to: _DocumentInput
};


export type MutationRemoveCompanyReferenced_InArgs = {
  from: _CompanyInput,
  to: _DocumentInput
};


export type MutationCreateCompanyArgs = {
  name?: Maybe<Scalars['String']>
};


export type MutationDeleteCompanyArgs = {
  name: Scalars['String']
};


export type MutationAddAssetRegistered_AtArgs = {
  from: _AssetInput,
  to: _AddressInput
};


export type MutationRemoveAssetRegistered_AtArgs = {
  from: _AssetInput,
  to: _AddressInput
};


export type MutationAddAssetReferenced_InArgs = {
  from: _AssetInput,
  to: _DocumentInput
};


export type MutationRemoveAssetReferenced_InArgs = {
  from: _AssetInput,
  to: _DocumentInput
};


export type MutationCreateAssetArgs = {
  name?: Maybe<Scalars['String']>,
  value?: Maybe<Scalars['String']>,
  type?: Maybe<Scalars['String']>,
  description?: Maybe<Scalars['String']>
};


export type MutationUpdateAssetArgs = {
  name: Scalars['String'],
  value?: Maybe<Scalars['String']>,
  type?: Maybe<Scalars['String']>,
  description?: Maybe<Scalars['String']>
};


export type MutationDeleteAssetArgs = {
  name: Scalars['String']
};


export type MutationAddAddressReferenced_InArgs = {
  from: _AddressInput,
  to: _DocumentInput
};


export type MutationRemoveAddressReferenced_InArgs = {
  from: _AddressInput,
  to: _DocumentInput
};


export type MutationCreateAddressArgs = {
  address?: Maybe<Scalars['String']>,
  city?: Maybe<Scalars['String']>,
  State?: Maybe<Scalars['String']>,
  country?: Maybe<Scalars['String']>
};


export type MutationUpdateAddressArgs = {
  address: Scalars['String'],
  city?: Maybe<Scalars['String']>,
  State?: Maybe<Scalars['String']>,
  country?: Maybe<Scalars['String']>
};


export type MutationDeleteAddressArgs = {
  address: Scalars['String']
};


export type MutationAddEventReferenced_InArgs = {
  from: _EventInput,
  to: _DocumentInput
};


export type MutationRemoveEventReferenced_InArgs = {
  from: _EventInput,
  to: _DocumentInput
};


export type MutationCreateEventArgs = {
  date?: Maybe<Scalars['String']>,
  Description?: Maybe<Scalars['String']>
};


export type MutationUpdateEventArgs = {
  date: Scalars['String'],
  Description?: Maybe<Scalars['String']>
};


export type MutationDeleteEventArgs = {
  date: Scalars['String']
};

export type Person = {
   __typename?: 'Person',
  name?: Maybe<Scalars['String']>,
  nationality?: Maybe<Scalars['String']>,
  country_of_residence?: Maybe<Scalars['String']>,
  dob?: Maybe<Scalars['String']>,
  shareholder_of?: Maybe<Array<Maybe<Company>>>,
  owner_of?: Maybe<Array<Maybe<Asset>>>,
  referenced_in?: Maybe<Array<Maybe<Document>>>,
  _id?: Maybe<Scalars['String']>,
};


export type PersonShareholder_OfArgs = {
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_CompanyOrdering>>>,
  filter?: Maybe<_CompanyFilter>
};


export type PersonOwner_OfArgs = {
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_AssetOrdering>>>,
  filter?: Maybe<_AssetFilter>
};


export type PersonReferenced_InArgs = {
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_DocumentOrdering>>>,
  filter?: Maybe<_DocumentFilter>
};

export type Query = {
   __typename?: 'Query',
  Document?: Maybe<Array<Maybe<Document>>>,
  Person?: Maybe<Array<Maybe<Person>>>,
  Company?: Maybe<Array<Maybe<Company>>>,
  Asset?: Maybe<Array<Maybe<Asset>>>,
  Address?: Maybe<Array<Maybe<Address>>>,
  Event?: Maybe<Array<Maybe<Event>>>,
};


export type QueryDocumentArgs = {
  title?: Maybe<Scalars['String']>,
  url?: Maybe<Scalars['String']>,
  publication_date?: Maybe<Scalars['String']>,
  _id?: Maybe<Scalars['String']>,
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_DocumentOrdering>>>,
  filter?: Maybe<_DocumentFilter>
};


export type QueryPersonArgs = {
  name?: Maybe<Scalars['String']>,
  nationality?: Maybe<Scalars['String']>,
  country_of_residence?: Maybe<Scalars['String']>,
  dob?: Maybe<Scalars['String']>,
  _id?: Maybe<Scalars['String']>,
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_PersonOrdering>>>,
  filter?: Maybe<_PersonFilter>
};


export type QueryCompanyArgs = {
  name?: Maybe<Scalars['String']>,
  _id?: Maybe<Scalars['String']>,
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_CompanyOrdering>>>,
  filter?: Maybe<_CompanyFilter>
};


export type QueryAssetArgs = {
  name?: Maybe<Scalars['String']>,
  value?: Maybe<Scalars['String']>,
  type?: Maybe<Scalars['String']>,
  description?: Maybe<Scalars['String']>,
  _id?: Maybe<Scalars['String']>,
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_AssetOrdering>>>,
  filter?: Maybe<_AssetFilter>
};


export type QueryAddressArgs = {
  address?: Maybe<Scalars['String']>,
  city?: Maybe<Scalars['String']>,
  State?: Maybe<Scalars['String']>,
  country?: Maybe<Scalars['String']>,
  _id?: Maybe<Scalars['String']>,
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_AddressOrdering>>>,
  filter?: Maybe<_AddressFilter>
};


export type QueryEventArgs = {
  date?: Maybe<Scalars['String']>,
  Description?: Maybe<Scalars['String']>,
  _id?: Maybe<Scalars['String']>,
  first?: Maybe<Scalars['Int']>,
  offset?: Maybe<Scalars['Int']>,
  orderBy?: Maybe<Array<Maybe<_EventOrdering>>>,
  filter?: Maybe<_EventFilter>
};


