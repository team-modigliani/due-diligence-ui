import {Observable} from '@apollo/client/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Company, SearchService } from './search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  companies: Observable<Company[]>;

  constructor(private searchService: SearchService, private fb: FormBuilder) { }

  searchForm = this.fb.group({
    search: ['']
  });

  ngOnInit() {
    // this.companies = this.searchService.watch({ search: this.searchForm.get('search') });
    // .valueChanges
    // .pipe(
    //     map(result => result.data.companies)
    //   );
  }

  add(): void {

  }
}
