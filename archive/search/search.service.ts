import {Query, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';



export interface Company {
  name: string;
  number: string;
  juristiction: string;
  status: string;
}

export interface Response {
  companies: Company[];
}

@Injectable({
  providedIn: 'root'
})

export class SearchService extends Query<Response> {
  document = gql`
    query getCompanies($search: String) {
      getCompanies(search: $search) {
        name
        number
        jurisdiction
        status
      }
    }
  `;
}
