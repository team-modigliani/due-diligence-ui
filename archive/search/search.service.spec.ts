import { TestBed } from '@angular/core/testing';

import { SearchService } from './search.service';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';

describe('SearchService', () => {
  let controller: ApolloTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ ApolloTestingModule ],
    });
    controller = TestBed.inject(ApolloTestingController);
  });

  it('should be created', () => {
    const service: SearchService = TestBed.inject(SearchService);
    expect(service).toBeTruthy();
  });
});
