export class Source {
    id: string;
    name: string;
    type: string;
    source: string;
    is_primary_source: boolean;

    constructor(id: string, name?: string, type?: string, source?: string, is_primary_source?: boolean) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.source = source;
        this.is_primary_source = is_primary_source;
    }
}