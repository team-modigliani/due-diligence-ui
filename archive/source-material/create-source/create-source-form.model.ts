import { FormControl } from '@angular/forms';
import { Source } from '../source.model';

export class SourceForm {
    id = new FormControl();
    name = new FormControl();
    type = new FormControl();
    source = new FormControl();
    is_primary_source = new FormControl();

    constructor(source: Source) {
        this.id.setValue(source.id);
        this.name.setValue(source.name);
        this.type.setValue(source.type);
        this.source.setValue(source.source);
        this.is_primary_source.setValue(!!source.is_primary_source);
    }
}