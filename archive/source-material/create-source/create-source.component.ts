import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-create-source',
  templateUrl: './create-source.component.html',
  styleUrls: ['./create-source.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateSourceComponent implements OnInit {
  @Input() sourceForm: FormGroup;
  @Input() index: number;
  @Output() deleteSource: EventEmitter<number> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  delete() {
    this.deleteSource.emit(this.index);
  }
}
