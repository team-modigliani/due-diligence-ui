import {Mutation, Apollo, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';



/*
  This is a note for future steve:
  Ideally this should be done with the use of gql fragments however there seems to be poor support
  for paramatized fragements and thus, in order to minimize duplication these are defined as strings instead
  and then imported into the gql that will execute them. It sucks but we need to make progress and not get too
  bogged down in the details.
*/
export const CREATE_SOURCE_MUTATION = `
  createSource: CreateSources(sources: $sources) {
    id
    name
    source
    type
    is_primary_source
  }
`;

@Injectable({
  providedIn: 'root'
})
export class CreateSourceService extends Mutation {
  constructor(apollo: Apollo) {
    super(apollo); // <-- use this to prevent the error from throwing
  }

  document = gql`mutation CreateSource($source_id: ID!, $source_id: ID!, $person_source_id: ID!) {
    ${CREATE_SOURCE_MUTATION}
  }`;
}
