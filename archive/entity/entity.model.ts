export class Entity {
    name: string;
    alias: string;
    status: string;
    type: string;
    creationDate: string;
    cessationDate: string;
}
