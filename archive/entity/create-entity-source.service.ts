import {Mutation, Apollo, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';



export const ADD_ENTITY_SOURCE_MUTATION = `
  AddEntitySource(from: {id: $entity_id}, to: {id: $source_id}, data: {id: $entity_source_id}) {
    id
  }
`;

@Injectable({
  providedIn: 'root'
})
export class CreateEntitySourceService extends Mutation {

  constructor(apollo: Apollo) {
    super(apollo);
  }

  document = gql`mutation AddEntitySource($entity_id: ID!, $source_id: ID!, $entity_source_id: ID!) {
    createEntitySource: ${ADD_ENTITY_SOURCE_MUTATION}
  }`;
}
