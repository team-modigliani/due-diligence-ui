import {Mutation, Apollo, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';



export const ADD_ENTITY_PROJECT_MUTATION = `
  AddEntityProject(from: {id: $entity_id}, to: {id: $project_id}, data: {id: $entity_project_id}) {
    id
  }
`;

@Injectable({
  providedIn: 'root'
})
export class CreateEntityProjectService extends Mutation {

  constructor(apollo: Apollo) {
    super(apollo);
  }

  document = gql`mutation AddEntityProject($entity_id: ID!, $project_id: ID!, $entity_project_id: ID!) {
    createEntityProject: ${ADD_ENTITY_PROJECT_MUTATION}
  }`;
}
