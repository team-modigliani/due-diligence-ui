import { TestBed } from '@angular/core/testing';

import { CreateEntitySourceService } from './create-entity-source.service';

describe('CreateEntitySourceService', () => {
  let service: CreateEntitySourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreateEntitySourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
