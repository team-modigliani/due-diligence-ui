import { TestBed } from '@angular/core/testing';

import { CreateEntityProjectService } from './create-entity-project.service';

describe('CreateEntityProjectService', () => {
  let service: CreateEntityProjectService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreateEntityProjectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
