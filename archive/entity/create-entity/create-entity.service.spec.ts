import { TestBed } from '@angular/core/testing';

import { CreateEntityService, CREATE_ENTITY_MUTATION } from './create-entity.service';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';

describe('CreateEntityService', () => {
  let controller: ApolloTestingController;
  let entityService: CreateEntityService;

  const dataIn = {
    name: 'Create entity name',
    alias: 'CEN',
    status: 'Active',
    type: 'PLC',
    creation_date: '2020-01-10',
    cessation_date: '2020-01-11'
  };

  const dataOut = {
    id: '123456-abc-789',
    name: 'Create entity name',
    alias: 'CEN',
    status: 'Active',
    type: 'PLC',
    creation_date: '',
    cessation_date: '',
    node_created: '1976-11-14',
    node_updated: '1976-11-14'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ ApolloTestingModule ],
    });

    controller = TestBed.inject(ApolloTestingController);
    entityService = TestBed.inject(CreateEntityService);
  });

  afterEach(() => {
    controller.verify();
  });

  it('should be created', () => {
    expect(entityService).toBeTruthy();
  });

  it('should collect values supplied', (done: DoneFn) => {
    entityService.mutate(dataIn).subscribe((entity: any) => {
      expect(entity.data.CreateEntity.id).toEqual('123456-abc-789');
      done();
    });

    const op = controller.expectOne(CREATE_ENTITY_MUTATION);
    expect(op.operation.variables.name).toEqual('Create entity name');
    expect(op.operation.variables.alias).toEqual('CEN');
    expect(op.operation.variables.status).toEqual('Active');
    expect(op.operation.variables.type).toEqual('PLC');
    expect(op.operation.variables.creation_date).toEqual('2020-01-10');
    expect(op.operation.variables.cessation_date).toEqual('2020-01-11');

    op.flush({
      data: {
        CreateEntity: dataOut
      }
    });
  });

  it('should return an ID', (done: DoneFn) => {
    entityService.mutate(dataIn).subscribe((entity: any) => {
      expect(entity.data.CreateEntity.id).toEqual('123456-abc-789');
      done();
    });

    const op = controller.expectOne(CREATE_ENTITY_MUTATION);

    op.flush({
      data: {
        CreateEntity: dataOut
      }
    });
  });

  it('should return a node_created value', (done: DoneFn) => {
    entityService.mutate(dataIn).subscribe((entity: any) => {
      expect(entity.data.CreateEntity.node_created).toEqual('1976-11-14');
      done();
    });

    const op = controller.expectOne(CREATE_ENTITY_MUTATION);

    op.flush({
      data: {
        CreateEntity: dataOut
      }
    });
  });
});
