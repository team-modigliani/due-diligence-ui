import {Mutation, Apollo, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';


import { CREATE_SOURCE_MUTATION } from '../../source-material/create-source/create-source.service';
import { ADD_ENTITY_PROJECT_MUTATION } from '../create-entity-project.service';
import { ADD_ENTITY_SOURCE_MUTATION } from '../create-entity-source.service';
export const CREATE_ENTITY_MUTATION = `
  createEntity: CreateEntity(id: $entity_id, name: $name, alias: $alias, status: $status, type: $type,
  creation_date: $creation_date, cessation_date: $cessation_date) {
    id
    name
    alias
    status
    type
    creation_date
    cessation_date
  }
`;

@Injectable({providedIn: 'root'})
export class CreateEntityService extends Mutation {
  constructor(apollo: Apollo) {
    super(apollo); // <-- use this to prevent the error from throwing
  }

  document = gql`mutation CreateEntity($project_id: ID!,
    $entity_id: ID!, $name: String, $alias: String, $status: String,
    $type: String, $creation_date: String, $cessation_date: String,
    $source_id: ID!, $source_name: String, $source_type: String, $source: String, $is_primary_source: Boolean,
    $entity_project_id: ID!, $entity_source_id: ID!) {
      ${CREATE_ENTITY_MUTATION}
      ${CREATE_SOURCE_MUTATION}
      ${ADD_ENTITY_PROJECT_MUTATION}
      ${ADD_ENTITY_SOURCE_MUTATION}
    }
  `;
}
