import { Component, OnInit } from '@angular/core';
import { CreateEntityService } from './create-entity.service';
import { FormBuilder, Validators } from '@angular/forms';
import { MessageService } from '../../message/message.service';
import { CreateUuidService } from '../../_services/create-uuid.service';
import { ActiveProjectService } from '../../_services/active-project.service';

@Component({
  selector: 'app-create-entity',
  templateUrl: './create-entity.component.html',
  styleUrls: ['./create-entity.component.scss']
})

export class CreateEntityComponent implements OnInit {
  project: any;
  entityForm = this.fb.group({
    name: ['', {updateOn: 'blur'}, [Validators.required]],
    alias: [''],
    status: [''],
    type: [''],
    creationDate: [''],
    cessationDate: [''],
    source: this.fb.group({
      sourceName: [''],
      sourceType: [''],
      sourceURI: [''],
      isPrimarySource: ['']
    })
  });

  create(): void {
    if (this.entityForm.valid) {
      this.createUuidService.mutate({
        count: 4
      })
      .subscribe(({data}: any) => {
        const ids = data.CreateUUID.uuid;
        this.createEntityService.mutate({
          project_id: this.project.id,
          entity_id: ids[0],
          name: this.entityForm.get('name').value,
          alias: this.entityForm.get('alias').value,
          status: this.entityForm.get('status').value,
          type: this.entityForm.get('type').value,
          creation_date: this.entityForm.get('creationDate').value,
          cessation_date: this.entityForm.get('cessationDate').value,
          source_id: ids[1],
          source_name: this.entityForm.get('source.sourceName').value,
          source_type: this.entityForm.get('source.sourceType').value,
          source: this.entityForm.get('source.sourceURI').value,
          is_primary_source: !!this.entityForm.get('source.isPrimarySource').value,
          entity_project_id: ids[2],
          entity_source_id: ids[3]
        })
        .subscribe(({data}: any) => {
          this.messageService.success(`Created a new entity node. The id is: ${data.CreateEntity.id}`);
        }, (error) => {
          this.messageService.error(`There was a problem creating the entity node: ${error}`);
        });
      });
    }
  }

  get form() { return this.entityForm.controls; }

  constructor(private createEntityService: CreateEntityService, private fb: FormBuilder, private messageService: MessageService, 
    private createUuidService: CreateUuidService, private projectService: ActiveProjectService) { }

  ngOnInit() {
    this.projectService.currentProject.subscribe(project => this.project = project);
  }

}
