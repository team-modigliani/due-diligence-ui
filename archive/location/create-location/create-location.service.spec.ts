import { TestBed } from '@angular/core/testing';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';

import { CreateLocationService, CREATE_LOCATION_MUTATION } from './create-location.service';

describe('CreateLocationService', () => {
  let controller: ApolloTestingController;
  let locationService: CreateLocationService;

  const dataIn = {
    name: 'Create location name',
    address: '',
    city: '',
    state: '',
    postcode: '',
    country: '',
    geospatial_coordinates: ''
  };

  const dataOut = {
    id: '123456789',
    name: 'Create location name',
    address: '',
    city: '',
    state: '',
    postcode: '',
    country: '',
    geospatial_coordinates: ''
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ ApolloTestingModule ],
    });

    controller = TestBed.inject(ApolloTestingController);
    locationService = TestBed.inject(CreateLocationService);
  });

  afterEach(() => {
    controller.verify();
  });

  it('should be created', () => {
    expect(locationService).toBeTruthy();
  });

  it('should collect values supplied', (done: DoneFn) => {
    locationService.mutate(dataIn).subscribe((entity: any) => {
      expect(entity.data.CreateLocation.id).toEqual('123456789');
      done();
    });

    const op = controller.expectOne(CREATE_LOCATION_MUTATION);

    op.flush({
      data: {
        CreateLocation: dataOut
      }
    });
  });
});
