import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreateLocationComponent } from './create-location.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { ComponentFixtureAutoDetect } from '@angular/core/testing';

describe('CreateLocationComponent', () => {
  let component: CreateLocationComponent;
  let fixture: ComponentFixture<CreateLocationComponent>;
  let controller: ApolloTestingController;
  let element: HTMLElement;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateLocationComponent ],
      imports: [FormsModule, ReactiveFormsModule, ApolloTestingModule, RouterTestingModule],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true }
      ]
    })
    .compileComponents();
    controller = TestBed.inject(ApolloTestingController);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
    debugElement = fixture.debugElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain a header', () => {
    expect(element.querySelector('h2').textContent).toContain('Create Location');
  });

  it('should have a disabled button on initial load', () => {
    expect(element.querySelector('button').getAttribute('disabled')).toBe('');
  });

  it('Should have validation on entity name', () => {
    const input = debugElement.query(By.css('#name'));
    const inputElement = input.nativeElement;

    expect(input.classes['ng-invalid']).toBeTruthy();

    inputElement.value = 'Entity';
    inputElement.dispatchEvent(new Event('input'));
    inputElement.dispatchEvent(new Event('blur'));

    expect(input.classes['ng-invalid']).toBeFalsy();
    expect(debugElement.query(By.css('button')).attributes['disabled']).toBeUndefined();
  });
});
