import { Component, OnInit } from '@angular/core';
import { CreateLocationService } from './create-location.service';
import { FormBuilder, Validators } from '@angular/forms';
import { MessageService } from '../../message/message.service';

@Component({
  selector: 'app-create-location',
  templateUrl: './create-location.component.html',
  styleUrls: ['./create-location.component.scss']
})

export class CreateLocationComponent implements OnInit {
  locationForm = this.fb.group({
    name: ['', {updateOn: 'blur'}, [Validators.required]],
    address: [''],
    city: [''],
    state: [''],
    country: [''],
    postcode: [''],
    coordinates: ['']
  });

  create() {
    if (this.locationForm.valid) {
      this.createLocationService.mutate({
        name: this.locationForm.get('name').value,
        address: this.locationForm.get('address').value,
        city: this.locationForm.get('city').value,
        state: this.locationForm.get('state').value,
        postcode: this.locationForm.get('postcode').value,
        country: this.locationForm.get('country').value,
        geospatial_coordinates: this.locationForm.get('coordinates').value
      })
      .subscribe(({data}: any) => {
        this.messageService.success(`Created a new location node. The id is: ${data.CreateLocation.id}`);
      }, (error) => {
        this.messageService.error(`There was a problem creating the entity node: ${error}`);
      });
    }
  }

  get form() { return this.locationForm.controls; }

  constructor(private createLocationService: CreateLocationService, private fb: FormBuilder, private messageService: MessageService) { }

  ngOnInit() {
  }

}
