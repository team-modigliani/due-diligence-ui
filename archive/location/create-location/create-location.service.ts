import {Mutation, Apollo, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';



export const CREATE_LOCATION_MUTATION = gql`
mutation CreateLocation($name: String, $address: String, $city: String,
$state: String, $postcode: String, $country: String, $geospatial_coordinates: String) {
  CreateLocation(name: $name, address: $address, city: $city, state: $state,
  postcode: $postcode, country: $country, geospatial_coordinates: $geospatial_coordinates) {
    id
    name
    address
    city
    state
    postcode
    country
    geospatial_coordinates
  }
}
`;

@Injectable({providedIn: 'root'})
export class CreateLocationService extends Mutation {
  constructor(apollo: Apollo) {
    super(apollo); // <-- use this to prevent the error from throwing
  }

  document = CREATE_LOCATION_MUTATION;
}
