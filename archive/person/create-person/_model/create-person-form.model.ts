
import { FormArray, FormControl } from '@angular/forms';
import { Person } from '../../person.model';

export class PersonForm {
    name = new FormControl();
    nationality = new FormControl();
    dob = new FormControl();
    dod = new FormControl();
    sources = new FormArray([]);

    constructor(person: Person) {
        if (person.name) {
            this.name.setValue(person.name);
        }

        if (person.nationality) {
            this.nationality.setValue(person.nationality);
        }

        if (person.dob) {
            this.dob.setValue(person.dob);
        }

        if (person.dod) {
            this.dod.setValue(person.dod);
        }

        if (person.sources) {
            this.sources.setValue(person.sources);
        }
    }
}