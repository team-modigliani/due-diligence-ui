import {Mutation, Apollo, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';



import { CREATE_SOURCE_MUTATION } from '../../source-material/create-source/create-source.service';
import { ADD_PERSON_PROJECT_MUTATION } from '../create-person-project.service';
import { ADD_PERSON_SOURCE_MUTATION } from '../create-person-source.service';

/*
  This is a note for future steve:
  Ideally this should be done with the use of gql fragments however there seems to be poor support
  for paramatized fragements and thus, in order to minimize duplication these are defined as strings instead
  and then imported into the gql that will execute them. It sucks but we need to make progress and not get too
  bogged down in the details.
*/
export const CREATE_PERSON_MUTATION = `
  createPerson: CreatePerson(id: $person_id, name: $name, nationality: $nationality, dob: $dob, dod: $dod) {
    id
    name
    nationality
    dob
    dod
  }
`;

@Injectable({
  providedIn: 'root'
})
export class CreatePersonService extends Mutation {
  constructor(apollo: Apollo) {
    super(apollo); // <-- use this to prevent the error from throwing
  }

  document = gql`mutation CreatePerson($project_id: ID!, $person_id: ID!, $name: String, $nationality: String, $dob: String, $dod: String,
    $sources: [SourceInput], $person_project_id: ID!) {
      ${CREATE_PERSON_MUTATION}
      ${CREATE_SOURCE_MUTATION}
      ${ADD_PERSON_PROJECT_MUTATION}
      ${ADD_PERSON_SOURCE_MUTATION}
  }`;
}
