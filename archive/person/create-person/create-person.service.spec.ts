import { TestBed } from '@angular/core/testing';

import { CreatePersonService } from './create-person.service';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';

describe('CreatePersonService', () => {
  let controller: ApolloTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ ApolloTestingModule ],
    });
    controller = TestBed.inject(ApolloTestingController);
  });

  it('should be created', () => {
    const service: CreatePersonService = TestBed.inject(CreatePersonService);
    expect(service).toBeTruthy();
  });
});

