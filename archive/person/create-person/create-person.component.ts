import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';
import { ActiveProjectService } from '../../_services/active-project.service';
import { Subscription } from 'rxjs';
import { CreatePersonFormService } from './create-person-form.service';

@Component({
  selector: 'app-create-person',
  templateUrl: './create-person.component.html',
  styleUrls: ['./create-person.component.scss']
})
export class CreatePersonComponent implements OnInit, OnDestroy {
  project: any;
  personForm: FormGroup;
  personFormSub: Subscription;
  sources: FormArray;

  constructor(private projectService: ActiveProjectService,
              private createPersonFormService: CreatePersonFormService) {
  }

  ngOnInit() {
    this.personFormSub = this.createPersonFormService.personForm$.subscribe(person => {
      this.personForm = person;
      this.sources = this.personForm.get('sources') as FormArray;
    });

    this.createPersonFormService.addSource();
    this.projectService.currentProject.subscribe(project => this.project = project);
  }

  ngOnDestroy() {
    this.personFormSub.unsubscribe();
  }

  create(): void {
    this.createPersonFormService.savePerson(this.project.id);
  }

  addSource() {
    this.createPersonFormService.addSource();
  }

  deleteSource(index: number) {
    this.createPersonFormService.deleteSource(index);
  }
}
