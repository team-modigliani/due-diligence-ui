import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { PersonForm } from './_model/create-person-form.model';
import { Person } from '../person.model';
import { Source } from '../../source-material/source.model';
import { SourceForm } from '../../source-material/create-source/create-source-form.model';
import { CreateUuidService } from '../../_services/create-uuid.service';
import { MessageService } from '../../message/message.service';
import { CreatePersonService } from './create-person.service';
import { switchMap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class CreatePersonFormService {
  private personForm: BehaviorSubject<FormGroup | undefined> =
    new BehaviorSubject(this.formBuilder.group(new PersonForm(new Person('', '', '', '', []))));

  personForm$: Observable<FormGroup> = this.personForm.asObservable();

  constructor(private formBuilder: FormBuilder, private uuid: CreateUuidService, private messageService: MessageService,
              private createPersonService: CreatePersonService) { }

  savePerson(projectId: string) {
    const currentPerson = this.personForm.getValue();
    const currentSources = currentPerson.get('sources') as FormArray;

    if (currentPerson.valid) {
      this.uuid.mutate({count: 2}).pipe(
        switchMap(({data}: any) => this.createPersonService.mutate({
          project_id: projectId,
          person_id: data.CreateUUID.uuid[0],
          name: currentPerson.get('name').value,
          nationality: currentPerson.get('nationality').value,
          dob: currentPerson.get('dob').value,
          dod: currentPerson.get('dob').value,
          sources: currentSources.value,
          person_project_id: `${data.CreateUUID.uuid[0]}-${projectId}`,
        }))
      ).subscribe(mutateResponse => {
        this.messageService.success(`Created a new person node ${mutateResponse}`);
      }, (error) => {
        this.messageService.error(`There was a problem creating the person node: ${error}`);
      });
    }
  }

  addSource() {
    const currentPerson = this.personForm.getValue();
    const currentSources = currentPerson.get('sources') as FormArray;

    this.uuid.mutate({count: 1}).toPromise().then(({data}: any) => {
      currentSources.push(this.formBuilder.group(new SourceForm(new Source(data.CreateUUID.uuid[0]))));
      this.personForm.next(currentPerson);
    });
  }

  deleteSource(i: number) {
    const currentPerson = this.personForm.getValue();
    const currentSources = currentPerson.get('sources') as FormArray;

    currentSources.removeAt(i);

    this.personForm.next(currentPerson);
  }
}
