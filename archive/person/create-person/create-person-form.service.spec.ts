import { TestBed } from '@angular/core/testing';
import { CreatePersonFormService } from './create-person-form.service';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';

describe('CreatePersonFormService', () => {
  let service: CreatePersonFormService;
  let controller: ApolloTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ ApolloTestingModule ],
    });
    service = TestBed.inject(CreatePersonFormService);
    controller = TestBed.inject(ApolloTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
