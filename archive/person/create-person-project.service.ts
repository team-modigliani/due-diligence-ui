import {Mutation, Apollo, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';



export const ADD_PERSON_PROJECT_MUTATION = `
  AddPersonProject(from: {id: $person_id}, to: {id: $project_id}, data: {id: $person_project_id}) {
    id
  }
`;

@Injectable({
  providedIn: 'root'
})
export class CreatePersonProjectService extends Mutation{

  constructor(apollo: Apollo) {
    super(apollo);
  }

  document = gql`mutation AddPersonProject($person_id: ID!, $project_id: ID!, $person_project_id: ID!) {
    ${ADD_PERSON_PROJECT_MUTATION}
  }`;
}
