export class Person {
    id: string;
    name: string;
    nationality: string;
    dob: string;
    dod: string;

    constructor(id: string, name: string, nationality?: string, dob?: string, dod?: string) {
        this.id = id;
        this.name = name;
        this.nationality = nationality;
        this.dob = dob;
        this.dod = dod;
    }
}
