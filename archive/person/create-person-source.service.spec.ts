import { TestBed } from '@angular/core/testing';

import { CreatePersonSourceService } from './create-person-source.service';

describe('CreatePersonSourceService', () => {
  let service: CreatePersonSourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreatePersonSourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
