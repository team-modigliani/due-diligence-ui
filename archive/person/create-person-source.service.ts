import {Mutation, Apollo, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';



export const ADD_PERSON_SOURCE_MUTATION = `
  AddPersonSources(sources: $sources, person_id: $person_id) {
    id
  }
`;

@Injectable({
  providedIn: 'root'
})
export class CreatePersonSourceService extends Mutation {

  constructor(apollo: Apollo) {
    super(apollo);
  }

  document = gql`mutation AddPersonSource($person_id: ID!, $source_id: ID!, $person_source_id: ID!) {
    ${ADD_PERSON_SOURCE_MUTATION}
  }`;
}
