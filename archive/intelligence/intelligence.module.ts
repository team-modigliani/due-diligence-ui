import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateIntelligenceComponent } from './create-intelligence/create-intelligence.component';
import { SearchIntelligenceComponent } from './search-intelligence/search-intelligence.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IntelligenceRoutingModule } from './intelligence-routing.module';
import { IntelligenceComponent } from './intelligence/intelligence.component';


@NgModule({
  declarations: [
    IntelligenceComponent,
    SearchIntelligenceComponent,
    CreateIntelligenceComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    IntelligenceRoutingModule
  ]
})
export class IntelligenceModule { }
