import { TestBed } from '@angular/core/testing';

import { CreateIntelligenceService, CREATE_INTELLIGENCE_MUTATION } from './create-intelligence.service';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';

describe('CreateIntelligenceService', () => {
  let controller: ApolloTestingController;
  let intelligenceService: CreateIntelligenceService;

  const dataIn = {
    name: 'Intelligence',
    source: '',
    type: '',
    is_primary_source: 'true'
  };

  const dataOut = {
    id: '123456789',
    name: 'Intelligence',
    source: '',
    type: '',
    is_primary_source: 'true'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ ApolloTestingModule ],
    });

    controller = TestBed.inject(ApolloTestingController);
    intelligenceService = TestBed.inject(CreateIntelligenceService);
  });

  afterEach(() => {
    controller.verify();
  });

  it('should be created', () => {
    const service: CreateIntelligenceService = TestBed.inject(CreateIntelligenceService);
    expect(service).toBeTruthy();
  });

  it('should collect values supplied in the form', (done: DoneFn) => {
    intelligenceService.mutate(dataIn).subscribe((intelligence: any) => {
      expect(intelligence.data.CreateIntelligence.id).toEqual('123456789');
      done();
    });

    const op = controller.expectOne(CREATE_INTELLIGENCE_MUTATION);

    op.flush({
      data: {
        CreateIntelligence: dataOut
      }
    });
  });
});
