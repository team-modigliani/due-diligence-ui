import { Component, OnInit } from '@angular/core';
import { CreateIntelligenceService } from './create-intelligence.service';
import { MessageService } from '../../message/message.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-intelligence',
  templateUrl: './create-intelligence.component.html',
  styleUrls: ['./create-intelligence.component.scss']
})

export class CreateIntelligenceComponent implements OnInit {

  intelligenceForm = this.fb.group({
    name: ['', {updateOn: 'blur'}, [Validators.required]],
    source: ['', {updateOn: 'blur'}, [Validators.required]],
    type: [''],
    primarySource: ['']
  });

  create(): void {
    if (this.intelligenceForm.valid) {
      this.createIntelligenceService.mutate({
        name: this.intelligenceForm.get('name').value,
        source: this.intelligenceForm.get('source').value,
        type: this.intelligenceForm.get('type').value,
        is_primary_source: !!this.intelligenceForm.get('primarySource').value
      })
      .subscribe(({data}: any) => {
        this.messageService.success(`Created a new piece of intelligence on
           ${data.CreateIntelligence.node_created}.
           The id is: ${data.CreateIntelligence.id}`);
      }, (error) => {
        this.messageService.error(`There was a problem creating this intelligence: ${error}`);
      });
    }
  }

  get form() { return this.intelligenceForm.controls; }

  constructor(private createIntelligenceService: CreateIntelligenceService,
              private fb: FormBuilder, private messageService: MessageService) { }

  ngOnInit() {
  }

}
