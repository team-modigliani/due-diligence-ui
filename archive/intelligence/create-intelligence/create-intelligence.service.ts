import {Mutation, Apollo, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';



export const CREATE_INTELLIGENCE_MUTATION = gql`
mutation CreateIntelligence($name: String,
  $source: String, $type: String, $is_primary_source: Boolean) {
  CreateIntelligence(name: $name, source: $source, type: $type,
  is_primary_source: $is_primary_source) {
    id
    name
    source
    type
    is_primary_source
  }
}
`;

@Injectable({ providedIn: 'root' })
export class CreateIntelligenceService extends Mutation {
  constructor(apollo: Apollo) {
    super(apollo); // <-- use this to prevent the error from throwing
  }

  document = CREATE_INTELLIGENCE_MUTATION;
}
