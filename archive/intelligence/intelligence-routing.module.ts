import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchIntelligenceComponent } from './search-intelligence/search-intelligence.component';
import { CreateIntelligenceComponent } from './create-intelligence/create-intelligence.component';
import { IntelligenceComponent } from './intelligence/intelligence.component';


const intelligenceRoutes: Routes = [
    {
      path: '',
      component: IntelligenceComponent,
      children: [
        {
          path: 'create',
          component: CreateIntelligenceComponent,
        },
        {
          path: 'search',
          component: SearchIntelligenceComponent,
        }
      ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(intelligenceRoutes)
    ],
    exports: [
      RouterModule
    ]
  })
  export class IntelligenceRoutingModule { }
