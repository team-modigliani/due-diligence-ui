import {Query, Apollo, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';



export const SEARCH_INTELLIGENCE_MUTATION = gql`
query findIntelligence($searchTerm: String) {
  findIntelligence(searchTerm: $searchTerm) {
    name
    source
    type
  }
}
`;

@Injectable({ providedIn: 'root' })
export class SearchIntelligenceService extends Query<Response> {
  constructor(apollo: Apollo) {
    super(apollo); // <-- use this to prevent the error from throwing
  }

  document = SEARCH_INTELLIGENCE_MUTATION;
}
