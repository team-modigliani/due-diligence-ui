import { TestBed } from '@angular/core/testing';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';
import { SearchIntelligenceService } from './search-intelligence.service';

describe('SearchIntelligenceService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ ApolloTestingModule ],
  }));

  it('should be created', () => {
    const service: SearchIntelligenceService = TestBed.inject(SearchIntelligenceService);
    expect(service).toBeTruthy();
  });
});
