import {Observable} from '@apollo/client/core';
import { Component, OnInit } from '@angular/core';
import { SearchIntelligenceService } from './search-intelligence.service';
import { MessageService } from '../../message/message.service';
import { FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-search-intelligence',
  templateUrl: './search-intelligence.component.html',
  styleUrls: ['./search-intelligence.component.scss']
})

export class SearchIntelligenceComponent implements OnInit {
  searchIntelligenceForm = this.fb.group({
    search: ['', {updateOn: 'blur'}, [Validators.required]]
  });
  results: Observable<any>;

  search(): void {
    if (this.searchIntelligenceForm.valid) {
      this.searchIntelligenceService.fetch({
        searchTerm: this.searchIntelligenceForm.get('search').value
      })
      .subscribe(({data}: any) => {
        this.results = data.findIntelligence || [];
        // this.messageService.success(`Created a new piece of intelligence on
        //    ${data.CreateIntelligence.node_created}.
        //    The id is: ${data.CreateIntelligence.id}`);
      }, (error) => {
        this.messageService.error(`There was a problem creating this intelligence: ${error}`);
      });
    }
  }

  get form() { return this.searchIntelligenceForm.controls; }

  constructor(private searchIntelligenceService: SearchIntelligenceService,
              private fb: FormBuilder, private messageService: MessageService) { }

  ngOnInit() {
  }

}