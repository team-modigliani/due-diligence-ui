enum Status {
    Converted,
    Active,
    Receivership,
    VoluntaryArrangement,
    Liquidation,
    Transferred,
    ActiveStikeOff,
    Dissolved,
    Administration
}