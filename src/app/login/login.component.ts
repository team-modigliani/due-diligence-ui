import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { MessageService } from '../message/message.service';
import { ClientService } from '../_services/client.service';
import { ActiveNodesService } from '../_services/active-nodes.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: any = {};
  submitted = false;
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  authorized$: Observable<any>;
  subscription: Subscription;
  constructor(
    private authService: AuthService,
    private router: Router,
    private clientService: ClientService,
    private activeNodesService: ActiveNodesService) { }

  ngOnInit(): void {
    this.authService.isLoggedIn.subscribe((isLoggedIn) => {
      if (isLoggedIn) {
        const user = this.authService.getCurrentUser();

        this.clientService.getClient(user.group).subscribe(client => {
          this.activeNodesService.changeClient(client);
        });

        if (user.group) {
          this.router.navigate([`/client/${user.group}`]);
        } else {
          this.router.navigate([`/client`]);
        }
      }
    });
  }

  onSubmit(): void {
    this.authService.login(this.form.username, this.form.password);
  }
}
