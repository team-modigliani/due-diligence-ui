import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { SectionService } from '../../_services/section.service';
import { ActiveNodesService } from '../../_services/active-nodes.service';

@Component({
  selector: 'app-reference-list',
  templateUrl: './reference-list.component.html',
  styleUrls: ['./reference-list.component.scss']
})
export class ReferenceListComponent implements OnInit {
  documents$: Observable<any>;

  constructor(
    private sectionService: SectionService, 
    private activeNodesService: ActiveNodesService) { }

  @Input()
  sectionId: any;

  ngOnInit(): void {
    this.documents$ = this.sectionService.getSectionDocuments(this.sectionId);

    this.activeNodesService.currentSection.subscribe(section => {
      this.documents$ = this.sectionService.getSectionDocuments(section.sectionId);
    });
  }
}
