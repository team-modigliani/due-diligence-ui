import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MockComponent } from 'ng-mocks';

import { ClientLayoutComponent } from './client-layout.component';
import { ClientHeaderComponent } from '../client-header/client-header.component';
import { SiteFooterComponent } from '../site-footer/site-footer.component';
import { MessageComponent } from '../../message/message.component';

describe('ClientLayoutComponent', () => {
  let component: ClientLayoutComponent;
  let fixture: ComponentFixture<ClientLayoutComponent>;

  beforeAll(() => {
    console.log('Client-layout test');
  });
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        ClientLayoutComponent,
        MockComponent(SiteFooterComponent),
        MockComponent(ClientHeaderComponent),
        MockComponent(MessageComponent)
      ],
      imports: [ RouterTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
