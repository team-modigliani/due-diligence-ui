import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MockComponent } from 'ng-mocks';

import { ReportLayoutComponent } from './report-layout.component';
import { ClientHeaderComponent } from '../client-header/client-header.component';
import { SiteFooterComponent } from '../site-footer/site-footer.component';
import { IntelligenceListComponent } from '../../intelligence/intelligence-list/intelligence-list.component';
import { ReportMenuComponent } from '../../report/report-menu/report-menu.component';
import { MessageComponent } from '../../message/message.component';

describe('ReportLayoutComponent', () => {
  let component: ReportLayoutComponent;
  let fixture: ComponentFixture<ReportLayoutComponent>;

  beforeAll(() => {
    console.log('report-layout test');
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ReportLayoutComponent,
        MockComponent(SiteFooterComponent),
        MockComponent(ClientHeaderComponent),
        MockComponent(IntelligenceListComponent),
        MockComponent(ReportMenuComponent),
        MockComponent(MessageComponent),
      ],
      imports: [ RouterTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
