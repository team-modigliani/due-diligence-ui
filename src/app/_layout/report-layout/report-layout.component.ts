import { Component, OnInit } from '@angular/core';
import { ActiveNodesService } from '../../_services/active-nodes.service';
import { Observable } from 'rxjs';

import $ from 'jquery';
@Component({
  selector: 'app-report-layout',
  templateUrl: './report-layout.component.html',
  styleUrls: ['./report-layout.component.scss']
})
export class ReportLayoutComponent implements OnInit {
  context$: Observable<any>;

  constructor(private activeNodesService: ActiveNodesService) { }

  ngOnInit(): void {
    this.context$ = this.activeNodesService.currentContext;
  }

}
