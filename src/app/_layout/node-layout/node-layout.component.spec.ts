import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MockComponent } from 'ng-mocks';

import { MessageComponent } from '../../message/message.component';
import { ClientHeaderComponent } from '../client-header/client-header.component';
import { NodeLayoutComponent } from './node-layout.component';

describe('NodeLayoutComponent', () => {
  let component: NodeLayoutComponent;
  let fixture: ComponentFixture<NodeLayoutComponent>;

  beforeAll(() => {
    console.log('node-layout test');
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NodeLayoutComponent,
        MockComponent(MessageComponent),
        MockComponent(ClientHeaderComponent)
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  afterAll(() => {
    console.log('jelly bean');
  })
});
