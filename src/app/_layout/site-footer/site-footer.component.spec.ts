import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { SiteFooterComponent } from './site-footer.component';

describe('SiteFooterComponent', () => {
  let component: SiteFooterComponent;
  let fixture: ComponentFixture<SiteFooterComponent>;
  let copyright: HTMLElement;

  beforeAll(() => {
    console.log('site-footer test');
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteFooterComponent ],
      imports: [ RouterTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a copyright notice', () => {
    copyright = fixture.nativeElement.querySelector('p');
    expect(copyright.textContent).toContain('© Steve Haffenden');
  });

  it('should have a copyright notice for the current year', () => {
    copyright = fixture.nativeElement.querySelector('p');
    expect(copyright.textContent).toContain(new Date().getUTCFullYear().toString());
  });
});
