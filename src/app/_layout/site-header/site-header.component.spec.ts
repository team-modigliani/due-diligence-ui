import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteHeaderComponent } from './site-header.component';

describe('SiteHeaderComponent', () => {
  let component: SiteHeaderComponent;
  let fixture: ComponentFixture<SiteHeaderComponent>;
  let branding: HTMLElement;

  beforeAll(() => {
    console.log('site-header.component test');
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display branding', () => {
    branding = fixture.nativeElement.querySelector('a.navbar-brand');
    expect(branding.textContent).toContain('Modigliani');
  });
});
