import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActiveNodesService } from '../../_services/active-nodes.service';
import { AuthService } from '../../_services/auth.service';
import { Observable } from 'rxjs';
import { Client } from '../../../generated/graphql';

@Component({
  selector: 'app-client-header',
  templateUrl: './client-header.component.html',
  styleUrls: ['./client-header.component.scss']
})
export class ClientHeaderComponent implements OnInit {
  currentClient$: Observable<Client>;
  loggedIn$: Observable<any>;

  constructor(
    private activeNodesService: ActiveNodesService,
    private router: Router,
    private authService: AuthService) { }

  ngOnInit() {
    this.loggedIn$ = this.authService.isLoggedIn;
    this.currentClient$ = this.activeNodesService.currentClient;
  }

  logout() {
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }
}
