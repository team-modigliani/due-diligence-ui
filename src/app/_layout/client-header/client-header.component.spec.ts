import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { asyncScheduler as _async} from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject } from 'rxjs';
import { ClientHeaderComponent } from './client-header.component';
import { ActiveNodesService } from '../../_services/active-nodes.service';
import { AuthService } from '../../_services/auth.service';
import { of } from 'rxjs';

class MockActiveNodesService {
  private client = new BehaviorSubject({clientId: 'cli1', name: 'ClientName'});
  currentClient = this.client.asObservable();
}

class MockAuthService {
  public isLoggedIn = of(true);
}

describe('ClientHeaderComponent', () => {
  let component: ClientHeaderComponent;
  let fixture: ComponentFixture<ClientHeaderComponent>;
  let branding: HTMLElement;

  beforeAll(() => {
    console.log('Client-header.component test');
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientHeaderComponent ],
      providers: [
        ClientHeaderComponent,
        { provide: ActiveNodesService, useClass: MockActiveNodesService},
        { provide: AuthService, useClass: MockAuthService }
      ],
      imports: [ RouterTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display the current client', () => {
    branding = fixture.nativeElement.querySelector('a.navbar-brand');
    expect(branding.innerHTML).toContain('ClientName');
  });
});
