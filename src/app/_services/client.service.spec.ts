import { TestBed } from '@angular/core/testing';
import { ClientListDocument, ClientDocument, ClientReportsDocument } from '../../generated/graphql';
import { ClientService } from './client.service';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';

describe('ClientService', () => {
  let service: ClientService;
  let controller: ApolloTestingController;
  const mockClient =  {
    clientId: 'cli1',
    name: 'Three Equity Chambers',
    __typename: 'Client'
  };

  beforeAll(() => {
    console.log('client.service test');
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ ApolloTestingModule ]
    });

    controller = TestBed.inject(ApolloTestingController);
    service = TestBed.inject(ClientService);
  });

  afterEach(() => {
    controller.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getClients Should return a list of clients', () => {
    service.getClients().subscribe(clients => {
      expect(clients[0].clientId).toEqual('cli1');
      expect(clients[0].name).toEqual('Three Equity Chambers');
    });

    const op = controller.expectOne(ClientListDocument);

    // Respond with mock data, causing Observable to resolve.
    op.flush({
      data: {
        Client: [mockClient]
      }
    });

    controller.verify();
  });

  it('getClient Should return a single clients', () => {
    service.getClient('cli1').subscribe(client => {
     expect(client.clientId).toEqual('cli1');
     expect(client.name).toEqual('Three Equity Chambers');
    });

    const op = controller.expectOne(ClientDocument);
    expect(op.operation.variables.clientId).toEqual('cli1');

    // Respond with mock data, causing Observable to resolve.
    op.flush({
      data: {
        Client: mockClient
      }
    });

    // Finally, assert that there are no outstanding operations.
    controller.verify();
  });

  it('getClientReports Should return a single clients', () => {
    service.getClientReports('cli1').subscribe(client => {
     expect(client.clientId).toEqual('cli1');
     expect(client.reports).toBeTruthy();
     expect(client.reports[0].reportId).toEqual('rep1');
    });

    const op = controller.expectOne(ClientReportsDocument);
    expect(op.operation.variables.clientId).toEqual('cli1');

    // Respond with mock data, causing Observable to resolve.
    op.flush({
      data: {
        Client: {
          clientId: 'cli1',
          name: 'Three Equity Chambers',
          __typename: 'Client',
          reports: [
            {
              reportId: 'rep1',
              name: 'Report1',
              avatar: '',
              status: 'in progress'
            },
            {
              reportId: 'rep2',
              name: 'Report1',
              avatar: '',
              status: 'in progress'
            }
          ]
        }
      }
    });

    // Finally, assert that there are no outstanding operations.
    controller.verify();
  });
});
