import { TestBed } from '@angular/core/testing';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';

import { CreateUuidService } from './create-uuid.service';


describe('UuidService', () => {
  let controller: ApolloTestingController;

  beforeAll(() => {
    console.log('create-uuid.service test');
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ ApolloTestingModule ]
    });

    controller = TestBed.inject(ApolloTestingController);
  });

  it('should be created', () => {
    const service: CreateUuidService = TestBed.inject(CreateUuidService);
    expect(service).toBeTruthy();
  });
});
