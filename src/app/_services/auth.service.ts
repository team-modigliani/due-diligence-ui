import { Injectable } from '@angular/core';
import { LoginGQL } from '../../generated/graphql';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MessageService } from '../message/message.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService  {
  private TOKEN_KEY = 'auth-token';
  private USER_KEY = 'auth-user';
  private login$: Observable<any>;

  private ln = null;
  private loggedIn = new BehaviorSubject(this.ln);

  isLoggedIn = this.loggedIn.asObservable();

  constructor(
    private loginGQL: LoginGQL,
    private messageService: MessageService
  ) { }

  public login(login: string, password: string): void {
    this.login$ = this.loginGQL.mutate({
      login,
      password
    }).pipe(map(result => result));

    this.login$.subscribe((result) => {
      if (result.data.Login) {
        this.setSession(result.data.Login.accessToken, JSON.stringify(result.data.Login.user));
        console.log('Gonna send a success message');
        this.messageService.success('You\'re logged in', {
          messageId: 'loginSuccess',
          autoClose: false,
          keepAfterRouteChange: true,
          fade: true
        });
      } else {
        this.clearSession();
        this.messageService.error('Your login failed. Please check your login details and try again', {
          messageId: 'loginFail',
          autoClose: false,
          fade: true
        });
      }
    });
  }

  public logout() {
    this.clearSession();
  }

  public updateSession() {
    const token = window.sessionStorage.getItem(this.TOKEN_KEY);
    if (token) {
      this.loggedIn.next(true);
    } else {
      this.clearSession();
    }
  }

  private setSession(token: string, user?: string) {
    window.sessionStorage.removeItem(this.TOKEN_KEY);
    window.sessionStorage.setItem(this.TOKEN_KEY, token);

    if (user) {
      window.sessionStorage.removeItem(this.USER_KEY);
      window.sessionStorage.setItem(this.USER_KEY, user);
    }

    this.loggedIn.next(true);
  }

  private clearSession() {
    window.sessionStorage.removeItem(this.TOKEN_KEY);
    window.sessionStorage.removeItem(this.USER_KEY);
    this.loggedIn.next(false);
  }

  public getCurrentUser() {
    const user = window.sessionStorage.getItem(this.USER_KEY);
    if (user) {
      return JSON.parse(user);
    } else {
      return {};
    }
  }
}
