import { Injectable } from '@angular/core';
import { Observable, of, interval } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { Client, Report, Intelligence, Investigation, Section } from '../../generated/graphql';

@Injectable({
  providedIn: 'root'
})
export class ActiveNodesService {
  private cl = {} as Client;
  private rp = {} as Report;
  private it = {} as Intelligence;
  private iv = {} as Investigation;
  private sc = {} as Section;
  private ct = 'intelligence';

  private client = new BehaviorSubject(this.cl);
  private report = new BehaviorSubject(this.rp);
  private intelligence = new BehaviorSubject(this.it);
  private investigation = new BehaviorSubject(this.iv);
  private section = new BehaviorSubject(this.sc);
  private sections = new BehaviorSubject([this.sc]);
  private subSections = new BehaviorSubject([this.sc]);
private context = new BehaviorSubject(this.ct);

  currentClient = this.client.asObservable();
  currentReport = this.report.asObservable();
  currentIntelligence = this.intelligence.asObservable();
  currentInvestigation = this.investigation.asObservable();
  currentSection = this.section.asObservable();
  currentSections = this.sections.asObservable();
  currentSubSections = this.subSections.asObservable();
  currentContext = this.context.asObservable();

  changeIntelligence(intelligence: any) {
    this.intelligence.next(intelligence);
  }

  changeClient(client: any) {
    this.client.next(client);
  }

  changeReport(report: any) {
    this.report.next(report);
  }

  changeInvestigation(investigation: any) {
    this.investigation.next(investigation);
  }

  changeSection(section: any) {
    this.section.next(section);
  }

  changeSubSection(sections: any) {
    this.subSections.next(sections);
  }

  changeSections(sections: any) {
    this.sections.next(sections);
  }

  changeContext(context: any) {
    this.context.next(context);
  }

  constructor() { }


}
