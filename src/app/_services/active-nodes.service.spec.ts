import { TestBed } from '@angular/core/testing';

import { ActiveNodesService } from './active-nodes.service';

describe('ActiveNodesService', () => {
  let service: ActiveNodesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ActiveNodesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
