import { Injectable } from '@angular/core';
import { Client, ClientGQL, ClientListGQL, ClientReportsGQL } from '../../generated/graphql';
import { ActiveNodesService } from './active-nodes.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(
    private clientGQL: ClientGQL,
    private clientListGQL: ClientListGQL,
    private clientReportsGQL: ClientReportsGQL,
    private activeNodesService: ActiveNodesService) { }

  private changeClient(client: Client): void {
    this.activeNodesService.changeClient(client);
  }

  getClients(): Observable<[Client]> {
    return this.clientListGQL.watch()
    .valueChanges.pipe(map(result => {
      this.changeClient(result.data.Client[0]);
      return result.data.Client as [Client];
    }));
  }

  getClient(clientId: string): Observable<Client> {
    return this.clientGQL.watch({ clientId })
    .valueChanges.pipe(map(result => {
      this.changeClient(result.data.Client[0]);
      return result.data.Client[0] as Client;
    }));
  }

  getClientReports(clientId: string): Observable<Client> {
    return this.clientReportsGQL.watch({ clientId })
      .valueChanges
      .pipe(map(result => result.data.Client[0] as Client)
    );
  }
}
