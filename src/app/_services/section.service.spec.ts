import { TestBed } from '@angular/core/testing';
import { SectionIntelligenceDocument } from '../../generated/graphql';
import { SectionService } from './section.service';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';


describe('SectionService', () => {
  let service: SectionService;
  let controller: ApolloTestingController;
  const mockSectionIntelligence = {
    Section: [
      {
        sectionId: 'sec1',
        intelligence: {
          intelligenceId: 'int1',
          nodes: [{
            __typename: 'Person',
            nodeId: 'per3',
            name: 'Benjamin Franklin'
          }],
          __typename: 'Intelligence'
        },
        __typename: 'Section'
      }
    ]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ ApolloTestingModule ]
    });

    controller = TestBed.inject(ApolloTestingController);
    service = TestBed.inject(SectionService);
  });

  beforeAll(() => {
    console.log('section.service test');
  });

  afterEach(() => {
    controller.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Should return a single section', () => {
    service.getSectionIntelligence('sec1').subscribe(section => {
      expect(section.sectionId).toEqual('sec1');
    });

    const op = controller.expectOne(SectionIntelligenceDocument);
    expect(op.operation.variables.sectionId).toEqual('sec1');

    op.flush({ data: { Section: mockSectionIntelligence } });

    controller.verify();
  });

  it('Should return an array of intelligence nodes', () => {
    return false;
  });

  it('Should return an array of documents nodes', () => {
    return false;
  });
});
