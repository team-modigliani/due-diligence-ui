import { Injectable } from '@angular/core';
import { SectionIntelligenceGQL, SectionDocumentsGQL, SectionCommentsGQL } from '../../generated/graphql';
import { ActiveNodesService } from './active-nodes.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SectionService {

  constructor(
    private sectionIntelligenceGQL: SectionIntelligenceGQL,
    private sectionDocumentsGQL: SectionDocumentsGQL,
    private sectionCommentsGQL: SectionCommentsGQL,
    private activeNodesService: ActiveNodesService) { }

    getSectionIntelligence(sectionId: string): Observable<any> {
      return this.sectionIntelligenceGQL.watch({ sectionId })
      .valueChanges
      .pipe(map(result => {
        this.activeNodesService.changeIntelligence(result.data.Section[0].intelligence);
        return result.data.Section[0].intelligence;
      }));
    }

    getSectionDocuments(sectionId: string): Observable<any> {
      return this.sectionDocumentsGQL.watch({ sectionId })
      .valueChanges
      .pipe(map(result => {
        return result.data.Section[0].intelligence.documents;
      }));
    }

    getSectionComments(sectionId: string): Observable<any> {
      return this.sectionCommentsGQL.watch({ sectionId })
      .valueChanges
      .pipe(map(result => {
        return result.data.Section[0].comments;
      }));
    }
}
