import { TestBed } from '@angular/core/testing';

import { TokenStorageService } from './token-storage.service';

describe('TokenStorageService', () => {
  let service: TokenStorageService;
  const TOKEN_KEY = 'auth-token';
  const USER_KEY = 'auth-user';
  const fakeToken = 'faketoken';
  const fakeUser = 'fakeuser';

  beforeAll(() => {
    console.log('token-storage.service test');
  });

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TokenStorageService);

    window.sessionStorage.removeItem(TOKEN_KEY);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should save a token in session storage', () => {
    service.saveToken(fakeToken);
    expect(service.getToken()).toEqual(window.sessionStorage.getItem(TOKEN_KEY));
  });

  it('should not alter the token', () => {
    service.saveToken(fakeToken);
    expect(service.getToken()).toEqual(fakeToken);
  });

  it('should be able to remove a token', () => {
    service.saveToken(fakeToken);
    expect(service.getToken()).toEqual(window.sessionStorage.getItem(TOKEN_KEY));

    service.clearToken();
    expect(service.getToken()).toBeFalsy();
  });

  it('should save a user to in session storage', () => {
    service.saveUser(fakeUser);
    expect(service.getUser()).toEqual(window.sessionStorage.getItem(USER_KEY));
  });

  it('should not alter the user', () => {
    service.saveUser(fakeUser);
    expect(service.getUser()).toEqual(fakeUser);
  });

  it('should clear the user', () => {
    service.saveUser(fakeUser);
    expect(service.getUser()).toEqual(window.sessionStorage.getItem(USER_KEY));

    service.clearToken();
    expect(service.getUser()).toBeFalsy();
  });
});
