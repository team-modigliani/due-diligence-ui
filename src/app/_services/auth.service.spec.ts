import { TestBed } from '@angular/core/testing';
import { LoginDocument } from '../../generated/graphql';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';

import { AuthService } from './auth.service';
import { MessageService } from '../message/message.service';
import { Observable, of } from 'rxjs';

describe('AuthService', () => {
  let service: AuthService;
  let controller: ApolloTestingController;

  const mockLogin =  {
    Login: {
      accessToken: 'abc123',
      user: {
        username: 'user',
        group: 'cli1'
      }
    }
  };

  beforeAll(() => {
    console.log('auth.service test');
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ ApolloTestingModule ],
    });

    controller = TestBed.inject(ApolloTestingController);
    service = TestBed.inject(AuthService);
  });

  afterEach(() => {
    controller.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Should mark user as logged in', () => {
    service.isLoggedIn.subscribe(loggedIn => {
      if (loggedIn) {
        expect(loggedIn).toBeTruthy();
      }
    });

    service.login('', '');
    const op = controller.expectOne(LoginDocument);

    op.flush({
      data: mockLogin
    });

    controller.verify();
  });

  it('Should create a session for the user once logged in', () => {
    service.isLoggedIn.subscribe(loggedIn => {
      if (loggedIn) {
        expect(loggedIn).toBeTruthy();
        expect(typeof service.getCurrentUser()).toEqual('object');
        expect(service.getCurrentUser().username).toEqual('user');
      }
    });

    service.login('', '');
    const op = controller.expectOne(LoginDocument);

    op.flush({
      data: mockLogin
    });

    controller.verify();
  });
});
