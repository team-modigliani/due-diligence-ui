import {Mutation, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';



@Injectable({
  providedIn: 'root'
})

export class CreateUuidService extends Mutation {
  document = gql`
    mutation CreateUUID($count: Int) {
      CreateUUID(count: $count) {
        uuid
      }
    }
  `;
}
