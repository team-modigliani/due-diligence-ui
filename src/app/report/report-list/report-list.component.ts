import { Component, OnInit, Input } from '@angular/core';
import { ActiveNodesService } from '../../_services/active-nodes.service';
import { Report } from '../../../generated/graphql';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-report-list',
  templateUrl: './report-list.component.html',
  styleUrls: ['./report-list.component.scss']
})
export class ReportListComponent implements OnInit {
  activeReport: Report;
  projectReports$: Observable<any>;

  @Input()
  sectionId: string;

  constructor(
    private activeNodesService: ActiveNodesService
  ) { }

  ngOnInit(): void {
    // this.activeProjectService.currentProject.subscribe(project => {
    //   this.activeProject = project;
    //   this.getReports();
    // });

    this.activeNodesService.currentReport.subscribe(report => {
      this.activeReport = report;
    });

    this.getReports();
  }

  switchReport(report): void {
    this.activeReport = report;
    this.activeNodesService.changeReport(report);
  }

  getReports(): void {
    // const rp = {} as Report;
    // this.projectReports$ = this.reportListService.watch({id: this.activeProject.id})
    // .valueChanges.pipe(
    //   map(result => {
    //     if (result.data.reports.length) {
    //       this.activeReportService.changeReport(rp);
    //       return result.data;
    //     } else {
    //       this.activeReportService.changeReport(rp);
    //     }
    //   })
    // );
  }
}
