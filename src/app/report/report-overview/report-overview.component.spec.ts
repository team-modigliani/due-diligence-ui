import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';

import { ReportOverviewComponent } from './report-overview.component';

describe('ReportOverviewComponent', () => {
  let component: ReportOverviewComponent;
  let fixture: ComponentFixture<ReportOverviewComponent>;
  let controller: ApolloTestingController;

  beforeAll(() => {
    console.log('report-overview.component test');
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportOverviewComponent ],
      imports: [ ApolloTestingModule ]
    })
    .compileComponents();

    controller = TestBed.inject(ApolloTestingController);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
