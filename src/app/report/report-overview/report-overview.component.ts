import { Component, OnInit, ComponentFactoryResolver, Input } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { SectionStatus, ReportOverviewGQL } from '../../../generated/graphql';
import showdown from 'showdown';

@Component({
  selector: 'app-report-overview',
  templateUrl: './report-overview.component.html',
  styleUrls: ['./report-overview.component.scss']
})
export class ReportOverviewComponent implements OnInit {
  report$: Observable<any>;
  content: string;

  @Input() reportId: any;

  constructor(
    private reportOverviewGQL: ReportOverviewGQL) { }
    private converter = new showdown.Converter();

  convertToHtml(report: any): any {
    return {
      reportId: report.reportId,
      mandate: report.mandate,
      sections: report.sections.map(item => ({
        sectionId: item.sectionId,
        name: item.name,
        content: this.converter.makeHtml(item.content)
      }))
    }
  }

  ngOnInit(): void {
    this.report$ = this.reportOverviewGQL.watch({id: this.reportId, filter: SectionStatus.Published })
        .valueChanges
        .pipe(map(result => this.convertToHtml(result.data.Report[0])
      )
    );
  }
}
