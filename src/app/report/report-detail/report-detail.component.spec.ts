import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { asyncScheduler as _async} from 'rxjs';
import { Observable, of, interval } from 'rxjs';
import { BehaviorSubject } from 'rxjs';

import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReportDetailComponent } from './report-detail.component';
import { ActiveNodesService } from '../../_services/active-nodes.service';

class MoackActiveNodesService {
  private client = new BehaviorSubject({clientId: 'cli1', name: 'Client'});
  currentClient = this.client.asObservable();

  private section = new BehaviorSubject({sectionId: 'sec1', name: 'Section'});
  currentSection = this.section.asObservable();

  changeSections() {}
  changeReport() {}
}

describe('ReportDetailComponent', () => {
  let component: ReportDetailComponent;
  let fixture: ComponentFixture<ReportDetailComponent>;
  let controller: ApolloTestingController;

  beforeAll(() => {
    console.log('report-detail.component test');
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportDetailComponent ],
      imports: [ ApolloTestingModule, RouterTestingModule ],
      providers: [ReportDetailComponent, { provide: ActiveNodesService, useClass: MoackActiveNodesService} ]
    })
    .compileComponents();

    controller = TestBed.inject(ApolloTestingController);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
