import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { ActiveNodesService } from '../../_services/active-nodes.service';

import showdown from 'showdown';
import { Report, Section, SectionStatus, ReportSectionsGQL } from '../../../generated/graphql';

@Component({
  selector: 'app-report-detail',
  templateUrl: './report-detail.component.html',
  styleUrls: ['./report-detail.component.scss']
})
export class ReportDetailComponent implements OnInit, OnDestroy {
  section: any;
  context: any;
  context$: Observable<any>;
  report$: Observable<any>;
  section$: Observable<any>;

  constructor(
    private reportSectionsGQL: ReportSectionsGQL,
    private route: ActivatedRoute,
    private activeNodesService: ActiveNodesService) { }
    private converter = new showdown.Converter();

  convertSection(section) {
    return {
      id: section.id,
      title: section.name,
      content: this.converter.makeHtml(section.content),
      sections: section.sections
    };
  }

  changeContext(context: string) {
    this.activeNodesService.changeContext(context);
  }

  ngOnInit(): void {
    this.section$ = this.activeNodesService.currentSection;
    this.context$ = this.activeNodesService.currentContext;

    this.report$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.reportSectionsGQL.watch({ id: params.get('id'), filter: SectionStatus.Published })
          .valueChanges
          .pipe(map(result => {
            const report = result.data.Report[0] as Report;
            console.log(report);
            this.activeNodesService.changeReport(report);
            this.activeNodesService.changeInvestigation(report.investigation);
            this.activeNodesService.changeSections(report.sections);
            this.activeNodesService.changeSection(report.sections[0]);
            return {
              Report: report,
            };
          }))
      )
    );

    this.activeNodesService.currentSection.subscribe(section => {
      this.section = {
        sectionId: section.sectionId,
        name: section.name,
        content: this.converter.makeHtml(section.content),
        sections: section.sections ? section.sections.map(item => this.convertSection(item)) : []
      };
    });
  }

  ngOnDestroy(): void {
    const rp = {} as Report;
    const sec = {} as Section;
    this.activeNodesService.changeSections([sec]);
    this.activeNodesService.changeReport(rp);
  }
}
