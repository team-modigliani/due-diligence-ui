import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ActiveNodesService } from '../../_services/active-nodes.service';

@Component({
  selector: 'app-report-menu',
  templateUrl: './report-menu.component.html',
  styleUrls: ['./report-menu.component.scss']
})
export class ReportMenuComponent implements OnInit {
  report$: Observable<any>;
  sections$: Observable<any>;

  sections: any;
  subSections: any;
  activeSection: any;
  activeReport: any;

  constructor(
    private activeNodesService: ActiveNodesService) { }

  switchSection(section: any): void {
    this.activeNodesService.changeSection(section);
  }

  scrollToTarget(section: any) {
    const elem = document.getElementById(section.id);
    elem.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});
  }

  ngOnInit(): void {
    this.activeNodesService.currentSection.subscribe(section => {
      this.activeSection = section;

      if (section.sections && section.sections.length) {
        this.activeNodesService.changeSubSection(section.sections);
      } else {
        this.activeNodesService.changeSubSection(null);
      }
    });

    this.activeNodesService.currentSections.subscribe(sections => {
      this.sections = sections;
    });

    this.activeNodesService.currentSubSections.subscribe(subSections => {
      this.subSections = subSections;
    });
  }
}
