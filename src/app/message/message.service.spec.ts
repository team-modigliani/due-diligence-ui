import { TestBed } from '@angular/core/testing';
import { MessageService } from './message.service';
import { RouterTestingModule } from '@angular/router/testing';

import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';

describe('MessageService', () => {
  let controller: ApolloTestingController;

  beforeAll(() => {
    console.log('Message.service test');
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ ApolloTestingModule, RouterTestingModule ],
    });
    controller = TestBed.inject(ApolloTestingController);
  });

  it('should be created', () => {
    // const service: MessageService = TestBed.inject(MessageService);
    // expect(service).toBeTruthy();
  });
});
