import { Injectable } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import { filter } from 'rxjs/operators';

import { Message, MessageType } from './message';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private subject = new Subject<Message>();
  private defaultId = 'defaultMessage';
  private messages: Message[] = [];

    // enable subscribing to messages observable
    onMessage(messageId?: string): Observable<Message> {
        return this.subject.asObservable().pipe(filter(x => {
            if (x && messageId) {
                return (x && x.messageId === messageId);
            } else {
                return true;
            }
        }));
    }

    // convenience methods
    success(message: string, options?: any) {
        this.message(new Message({ ...options, type: MessageType.Success, message }));
    }

    error(message: string, options?: any) {
        this.message(new Message({ ...options, type: MessageType.Error, message }));
    }

    info(message: string, options?: any) {
        this.message(new Message({ ...options, type: MessageType.Info, message}));
    }

    warn(message: string, options?: any) {
        this.message(new Message({ ...options, type: MessageType.Warning, message}));
    }

    // main message method
    message(message: Message) {
        message.messageId = message.messageId || this.defaultId;
        this.subject.next(message);
    }

    // clear messages
    clear(messageId?: string) {
        this.subject.next(new Message({ messageId }));
    }

    getMessages() {
      return this.messages;
    }

    setMessages(messages: [Message]) {
      this.messages = messages;
    }
}
