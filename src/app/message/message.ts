export class Message {
    type: MessageType;
    message: string;
    messageId: string;
    autoClose: boolean;
    fade: boolean;
    keepAfterRouteChange: boolean;

    constructor(init?: Partial<Message>) {
        Object.assign(this, init);
    }
}

export enum MessageType {
    Success,
    Error,
    Info,
    Warning
}
