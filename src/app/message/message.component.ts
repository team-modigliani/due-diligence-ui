import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, NavigationStart } from '@angular/router';
import { Message, MessageType } from './message';
import { MessageService } from './message.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit, OnDestroy {
  @Input() id: string;
  @Input() fade = true;

  messages: Message[] = [];
  subscription: Subscription;
  routeSubscription: Subscription;

  constructor(private router: Router, private messageService: MessageService) { }

  ngOnInit() {
    this.messages = this.messageService.getMessages();
    this.subscription = this.messageService.onMessage().subscribe(message => {
        if (!message.message) {
          this.messages = this.messages.filter(x => x.keepAfterRouteChange);
          this.messages.forEach(x => delete x.keepAfterRouteChange);
          return;
        }

        this.messages.push(message);

        if (message.autoClose) {
          setTimeout(() => this.removeMessage(message), 3000);
        }
      }
    );

    // clear alerts on location change
    this.routeSubscription = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.messageService.clear(this.id);
      }
    });
  }

  ngOnDestroy() {
      this.subscription.unsubscribe();
      //this.routeSubscription.unsubscribe();
  }

  removeMessage(message: Message) {
    if (!this.messages.includes(message)) {
      return;
    }

    if (this.fade) {
        this.messages.find(x => x === message).fade = true;

        setTimeout(() => {
            this.messages = this.messages.filter(x => x !== message);
        }, 250);
    } else {
        this.messages = this.messages.filter(x => x !== message);
    }
  }

  cssClass(message: Message) {
      if (!message) {
          return;
      }

      const classes = ['alert', 'alert-dismissable'];
      // return css class based on alert type
      switch (message.type) {
          case MessageType.Success:
              classes.push('alert-success');
              break;
          case MessageType.Error:
              classes.push('alert-danger');
              break;
          case MessageType.Info:
            classes.push('alert-info');
            break;
          case MessageType.Warning:
            classes.push('alert-warning');
            break;
      }

      if (message.fade) {
        classes.push('fade');
      }

      return classes.join(' ');
  }
}
