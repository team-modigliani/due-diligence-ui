import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { SectionService } from '../../_services/section.service';
import { ActiveNodesService } from '../../_services/active-nodes.service';

@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.scss']
})
export class CommentListComponent implements OnInit {
  comments$: Observable<any>;

  @Input() sectionId;

  constructor(
    private sectionService: SectionService,
    private activeNodesService: ActiveNodesService) { }

  ngOnInit(): void {
    this.comments$ = this.sectionService.getSectionComments(this.sectionId);

    this.activeNodesService.currentSection.subscribe(section => {
      this.comments$ = this.sectionService.getSectionComments(section.sectionId);

      this.comments$.subscribe(comments => {
        console.log(comments);
      })
    });
  }

}
