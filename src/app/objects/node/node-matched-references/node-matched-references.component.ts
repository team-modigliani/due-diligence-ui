import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-node-matched-references',
  templateUrl: './node-matched-references.component.html',
  styleUrls: ['./node-matched-references.component.scss']
})
export class NodeMatchedReferencesComponent implements OnInit {
  @Input() from;
  @Input() to;

  matchedReferences: any;
  private refSet: any = new Set();

  constructor() { }

  ngOnInit(): void {
    if (this.from.documents && this.to.documents) {
      this.from.documents.forEach(item => {
        const match = this.to.documents.find(obj => obj.Document.documentId === item.Document.documentId);

        if (match) {
          this.refSet.add(match);
        }
      });

      if (this.refSet.size > 0) {
        this.matchedReferences = new Array(...this.refSet);
      }
    }
  }
}
