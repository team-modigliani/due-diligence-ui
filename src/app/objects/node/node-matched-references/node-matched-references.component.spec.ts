import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';

import { NodeMatchedReferencesComponent } from './node-matched-references.component';

describe('NodeMatchedReferencesComponent', () => {
  let component: NodeMatchedReferencesComponent;
  let fixture: ComponentFixture<NodeMatchedReferencesComponent>;
  let controller: ApolloTestingController;

  beforeAll(() => {
    console.log('node-matched-references.component test');
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeMatchedReferencesComponent ],
      imports: [ ApolloTestingModule ]
    })
    .compileComponents();

    controller = TestBed.inject(ApolloTestingController);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeMatchedReferencesComponent);
    component = fixture.componentInstance;
    component.from = {
      nodeId: 'obj1'
    };
    component.to = {
      nodeId: 'obj2'
    };
    component.matchedReferences = [{
      context: '',
      Document: [{
        name: ''
      }]
    }];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
