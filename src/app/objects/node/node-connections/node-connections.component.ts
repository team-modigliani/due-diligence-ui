import { Component, OnInit } from '@angular/core';
import { ActiveNodesService } from '../../../_services/active-nodes.service';
import { NodeConnectionsGQL } from '../../../../generated/graphql';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ActivatedRoute, ParamMap } from '@angular/router';
@Component({
  selector: 'app-node-connections',
  templateUrl: './node-connections.component.html',
  styleUrls: ['./node-connections.component.scss']
})
export class NodeConnectionsComponent implements OnInit {
  connections$: Observable<any>;

  constructor(
    private activeNodesService: ActiveNodesService,
    private nodeConnectionsGQL: NodeConnectionsGQL,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.activeNodesService.currentInvestigation.subscribe(inv => {
      this.connections$ = this.route.paramMap.pipe(
        switchMap((params: ParamMap) =>
          this.nodeConnectionsGQL.watch({ investigationId: inv.investigationId, nodeId: params.get('id') })
          .valueChanges
          .pipe(map(result => {
            console.log('CONNECTIONS: ');
            console.log(result.data.Node[0]);
            return result.data.Node[0];
          }))
        )
      );
    });
  }

}
