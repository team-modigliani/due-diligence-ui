import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { asyncScheduler as _async} from 'rxjs';
import { Observable, of, interval } from 'rxjs';
import { BehaviorSubject } from 'rxjs';

import { ActiveNodesService } from '../../../_services/active-nodes.service';
import { NodeConnectionsComponent } from './node-connections.component';

class MockActiveNodesService {
  private client = new BehaviorSubject({clientId: 'cli1', name: 'Client'});
  currentClient = this.client.asObservable();

  private section = new BehaviorSubject({sectionId: 'sec1', name: 'Section'});
  currentSection = this.section.asObservable();

  private investigation = new BehaviorSubject({investigationId: 'inv1', name: 'Investigation'});
  currentInvestigation = this.investigation.asObservable();

  changeSections() {}
  changeReport() {}
}

describe('NodeConnectionsComponent', () => {
  let component: NodeConnectionsComponent;
  let fixture: ComponentFixture<NodeConnectionsComponent>;
  let controller: ApolloTestingController;

  beforeAll(() => {
    console.log('node-connections.component test');
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeConnectionsComponent ],
      imports: [ ApolloTestingModule, RouterTestingModule ],
      providers: [NodeConnectionsComponent, { provide: ActiveNodesService, useClass: MockActiveNodesService} ]

    })
    .compileComponents();

    controller = TestBed.inject(ApolloTestingController);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeConnectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
