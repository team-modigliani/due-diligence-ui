import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { NodeReferencesComponent } from './node-references.component';

describe('NodeReferencesComponent', () => {
  let component: NodeReferencesComponent;
  let fixture: ComponentFixture<NodeReferencesComponent>;

  beforeAll(() => {
    console.log('node-references.component test');
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeReferencesComponent ],
      imports: [ RouterTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeReferencesComponent);
    component = fixture.componentInstance;
    component.nodeReference = {
      documents: [{
        context: 'context',
        Document: {
          name: 'Name'
        }
      }]
    };

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
