import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-node-references',
  templateUrl: './node-references.component.html',
  styleUrls: ['./node-references.component.scss']
})
export class NodeReferencesComponent implements OnInit {
  @Input() nodeReference;

  constructor() { }

  ngOnInit(): void {
  }
}
