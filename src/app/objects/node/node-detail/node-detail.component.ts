import { Component, OnInit, Input } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { IntelligenceNodeGQL } from '../../../../generated/graphql';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { ActiveNodesService } from '../../../_services/active-nodes.service';
@Component({
  selector: 'app-node-detail',
  templateUrl: './node-detail.component.html',
  styleUrls: ['./node-detail.component.scss']
})
export class NodeDetailComponent implements OnInit {
  intelligence$: Observable<any>;

  constructor(
    private intelligenceNodeGQL: IntelligenceNodeGQL,
    private route: ActivatedRoute,
    private activeNodesService: ActiveNodesService) { }

  ngOnInit(): void {
    this.activeNodesService.currentIntelligence.subscribe(intel => {
      this.intelligence$ = this.route.paramMap.pipe(
        switchMap((params: ParamMap) =>
          this.intelligenceNodeGQL.watch({ intelligenceId: intel.intelligenceId, nodeId: params.get('id') })
          .valueChanges
          .pipe(map(result => {
            return result.data.Intelligence[0];
          }))
        )
      );
    });
  }
}
