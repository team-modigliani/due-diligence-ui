import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { NodeIconComponent } from './node-icon.component';

describe('NodeIconComponent', () => {
  let component: NodeIconComponent;
  let fixture: ComponentFixture<NodeIconComponent>;

  beforeAll(() => {
    console.log('node-icon.component test');
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeIconComponent ],
      imports: [ RouterTestingModule ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
