import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-node-icon',
  templateUrl: './node-icon.component.html',
  styleUrls: ['./node-icon.component.scss']
})
export class NodeIconComponent implements OnInit {

  @Input() type;
  @Input() size;

  constructor() { }

  ngOnInit(): void {
    this.type = this.type ? this.type.toLowerCase() : 'question';
    this.size = this.size ? this.size : '1x';
  }

}
