import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { map } from 'rxjs/operators';
import { ActiveNodesService } from '../../../../_services/active-nodes.service';
import { NodeConnectionsGQL } from '../../../../../generated/graphql';

@Component({
  selector: 'app-person-detail',
  templateUrl: './person-detail.component.html',
  styleUrls: ['./person-detail.component.scss']
})
export class PersonDetailComponent implements OnInit {
  @Input() person;

  constructor(private activeNodesService: ActiveNodesService, private nodeConnectionsGQL: NodeConnectionsGQL) { }

  ngOnInit(): void {
    this.activeNodesService.currentInvestigation.subscribe(investigation => {
      this.nodeConnectionsGQL.watch({ nodeId: this.person.nodeId, investigationId: investigation.investigationId })
        .valueChanges
        .pipe(map(result => {
          const nodes = result.data.Node;
          return {
            nodes
          }
        }));
    });
  }
}
