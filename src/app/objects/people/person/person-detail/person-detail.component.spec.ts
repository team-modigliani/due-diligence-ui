import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';
import { asyncScheduler as _async} from 'rxjs';
import { Observable, of, interval } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';

import { PersonDetailComponent } from './person-detail.component';
import { ActiveNodesService } from '../../../../_services/active-nodes.service';


class MockActiveNodesService {
  private client = new BehaviorSubject({clientId: 'cli1', name: 'Client'});
  currentClient = this.client.asObservable();

  private section = new BehaviorSubject({sectionId: 'sec1', name: 'Section'});
  currentSection = this.section.asObservable();

  private investigation = new BehaviorSubject({investigationId: 'inv1', name: 'Investigation'});
  currentInvestigation = this.investigation.asObservable();

  changeSections() {}
  changeReport() {}
}

describe('PersonDetailComponent', () => {
  let component: PersonDetailComponent;
  let fixture: ComponentFixture<PersonDetailComponent>;
  let controller: ApolloTestingController;

  beforeAll(() => {
    console.log('person-detail.component test');
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonDetailComponent ],
      imports: [ ApolloTestingModule, RouterTestingModule ],
      providers: [PersonDetailComponent, { provide: ActiveNodesService, useClass: MockActiveNodesService} ]

    })
    .compileComponents();

    controller = TestBed.inject(ApolloTestingController);

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonDetailComponent);
    component = fixture.componentInstance;
    component.person = {
      media: [{
        Media: {
          link: ''
        }
      }],
      name: '',
      dateOfBirth: {
        formatted: ''
      },
      nationalities: [],
      aliases: []
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
