import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-address-detail',
  templateUrl: './address-detail.component.html',
  styleUrls: ['./address-detail.component.scss']
})
export class AddressDetailComponent implements OnInit {
  @Input() address;

  constructor() { }

  ngOnInit(): void {
    console.log(this.address);
  }

}
