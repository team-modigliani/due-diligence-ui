export class Entity {
    id: string;
    name: string;
    alias: string;
    status: string;
    type: string;
    creationDate: string;
    cessationDate: string;

    constructor(id: string, name: string, alias?: string, status?: string, type?: string, creationDate?: string, cessationDate?: string) {
        this.id = id;
        this.name = name;
        this.alias = alias;
        this.status = status;
        this.type = type;
        this.creationDate = creationDate;
        this.cessationDate = cessationDate;
    }
}