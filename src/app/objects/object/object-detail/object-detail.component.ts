import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-object-detail',
  templateUrl: './object-detail.component.html',
  styleUrls: ['./object-detail.component.scss']
})
export class ObjectDetailComponent implements OnInit {

  @Input() objectDetail;

  constructor() { }

  ngOnInit(): void {
    console.log(this.objectDetail);
  }

}
