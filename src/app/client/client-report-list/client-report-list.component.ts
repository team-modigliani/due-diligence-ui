import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { ClientService } from '../../_services/client.service';
import { Client } from '../../../generated/graphql';
@Component({
  selector: 'app-client-report-list',
  templateUrl: './client-report-list.component.html',
  styleUrls: ['./client-report-list.component.scss']
})
export class ClientReportListComponent implements OnInit {
  client$: Observable<Client>;
  content: string;

  constructor(
    private route: ActivatedRoute,
    private clientService: ClientService
  ) { }

  ngOnInit(): void {
    console.log('client report list here');
    this.client$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.clientService.getClientReports(params.get('id')))
    );
  }
}
