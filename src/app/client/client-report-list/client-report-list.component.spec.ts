import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';
import { cold, getTestScheduler } from 'jasmine-marbles';

import { ClientReportListComponent } from './client-report-list.component';
import { ClientService } from '../../_services/client.service';

describe('ClientReportListComponent', () => {
  let component: ClientReportListComponent;
  let fixture: ComponentFixture<ClientReportListComponent>;
  let controller: ApolloTestingController;
  let clientService: any;
  let clientServiceSpy: jasmine.Spy;
  let element: HTMLElement;

  beforeAll(() => {
    console.log('Client-report-list.component test');
  });

  beforeEach(async(() => {
    clientService = jasmine.createSpyObj('ClientService', ['getClientReports']);
    clientServiceSpy = clientService.getClientReports;

    TestBed.configureTestingModule({
      declarations: [ ClientReportListComponent ],
      imports: [ RouterTestingModule, ApolloTestingModule ],
      providers: [
        ClientReportListComponent,
        { provide: ClientService, useValue: clientService }
      ]
    })
    .compileComponents();

    component = TestBed.inject(ClientReportListComponent);
    controller = TestBed.inject(ApolloTestingController);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientReportListComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display a list of reports if there are reports to display', () => {
    const q$ = cold('---x|', { x: {
      clientId: 'cli1',
      name: 'Three Equity Chambers',
      __typename: 'Client',
      reports: [
        {
          reportId: 'rep1',
          name: 'Report1',
          avatar: '',
          status: 'in progress'
        },
        {
          reportId: 'rep2',
          name: 'Report1',
          avatar: '',
          status: 'in progress'
        }
      ]
    }});

    clientServiceSpy.and.returnValue(q$);
    fixture.detectChanges();
    element = fixture.nativeElement.querySelector('h2');

    expect(element).toBeNull();

    getTestScheduler().flush();
    fixture.detectChanges();

    element = fixture.nativeElement.querySelector('h2');
    expect(element.textContent).toContain('Report list');

    element = fixture.nativeElement.querySelector('h5');
    expect(element.textContent).toContain('Report1');
  });

  it('should display a message if there are no reports to display', () => {
    const q$ = cold('---x|', { x: {
      clientId: 'cli1',
      name: 'Three Equity Chambers',
      __typename: 'Client',
      reports: []
    }});

    clientServiceSpy.and.returnValue(q$);
    fixture.detectChanges();

    getTestScheduler().flush();
    fixture.detectChanges();

    element = fixture.nativeElement.querySelector('h5');
    expect(element.textContent).toContain('No Reports Found');
  });

  it('should display a message if the reports are not returned', () => {
    const q$ = cold('---x|', { x: {
      clientId: 'cli1',
      name: 'Three Equity Chambers',
      __typename: 'Client',
    }});

    clientServiceSpy.and.returnValue(q$);
    fixture.detectChanges();

    getTestScheduler().flush();
    fixture.detectChanges();

    element = fixture.nativeElement.querySelector('h5');
    expect(element.textContent).toContain('No Reports Found');
  });

  it('should display an avatar if there is an avatar to display', () => {
    const q$ = cold('---x|', { x: {
      clientId: 'cli1',
      name: 'Three Equity Chambers',
      __typename: 'Client',
      reports: [
        {
          reportId: 'rep1',
          name: 'Report1',
          avatar: 'https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png',
          status: 'in progress'
        }
      ]
    }});

    clientServiceSpy.and.returnValue(q$);
    fixture.detectChanges();

    getTestScheduler().flush();
    fixture.detectChanges();

    element = fixture.nativeElement.querySelector('img');
    expect(element.outerHTML).not.toBeNull();
  });
});
