import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';
import { cold, getTestScheduler } from 'jasmine-marbles';

import { ClientListComponent } from './client-list.component';
import { TokenStorageService } from '../../_services/token-storage.service';
import { ClientService } from '../../_services/client.service';
import { Client } from '../../../generated/graphql';

describe('ClientListComponent', () => {
  let component: ClientListComponent;
  let fixture: ComponentFixture<ClientListComponent>;
  let controller: ApolloTestingController;
  let clientService: any;
  let clientServiceSpy: jasmine.Spy;
  let tokenStorageService: any;
  let tokenStorageServiceSpy: jasmine.Spy;
  let clients: HTMLElement;

  beforeAll(() => {
    console.log('client-list.component test');
  });

  beforeEach(async(() => {
    clientService = jasmine.createSpyObj('ClientService', ['getClients', 'getClient']);
    clientServiceSpy = clientService.getClients;

    tokenStorageService = jasmine.createSpyObj('TokenStorageService', ['getUser']);
    tokenStorageServiceSpy = tokenStorageService.getUser;

    TestBed.configureTestingModule({
      declarations: [ ClientListComponent ],
      providers: [
        ClientListComponent,
        { provide: TokenStorageService, useValue: tokenStorageService },
        { provide: ClientService, useValue: clientService }
      ],
      imports: [ ApolloTestingModule ]
    })
    .compileComponents();

    component = TestBed.inject(ClientListComponent);
    controller = TestBed.inject(ApolloTestingController);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientListComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should display a dropdown for ADMIN when there is more than 1 client', () => {
    const q$ = cold('---x|', {x: [
        { clientId: 'cli1', name: 'client1' } as Client,
        { clientId: 'cli2', name: 'client2' } as Client
     ]});

    tokenStorageServiceSpy.and.returnValue({ user: { role: 'ADMIN' } });
    clientServiceSpy.and.returnValue( q$ );

    fixture.detectChanges();
    clients = fixture.nativeElement.querySelector('.dropdown-item');
    expect(clients).toBeNull();

    getTestScheduler().flush();
    fixture.detectChanges();
    clients = fixture.nativeElement.querySelector('.dropdown-item');
    expect(clients.textContent).toContain('client1');
  });

  it('Should display nothing for ADMIN when there is only 1 client', () => {
    const q$ = cold('---x|', {x: [
        { clientId: 'cli1', name: 'client1' } as Client
     ]});

    tokenStorageServiceSpy.and.returnValue({ user: { role: 'ADMIN' } });
    clientServiceSpy.and.returnValue( q$ );

    fixture.detectChanges();

    clients = fixture.nativeElement.querySelector('.dropdown-item');
    expect(clients).toBeNull();

    getTestScheduler().flush();
    fixture.detectChanges();

    clients = fixture.nativeElement.querySelector('.dropdown-item');
    expect(clients).toBeNull();
  });

  it('Should not display a dropdown for USER', () => {
    const q$ = cold('---x|', {x: [
        { clientId: 'cli1', name: 'client1' } as Client,
        { clientId: 'cli2', name: 'client2' } as Client
     ]});

    tokenStorageServiceSpy.and.returnValue({ user: { role: 'USER' } });
    clientServiceSpy.and.returnValue( q$ );

    fixture.detectChanges();
    clients = fixture.nativeElement.querySelector('.dropdown-item');
    expect(clients).toBeNull();

    getTestScheduler().flush();
    fixture.detectChanges();

    clients = fixture.nativeElement.querySelector('.dropdown-item');
    expect(clients).toBeNull();
  });
});
