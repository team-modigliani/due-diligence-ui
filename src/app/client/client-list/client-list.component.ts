import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { TokenStorageService } from '../../_services/token-storage.service';
import { ClientService } from '../../_services/client.service';
import { Client } from '../../../generated/graphql';
import { ActiveNodesService } from 'src/app/_services/active-nodes.service';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit {
  clients$: Observable<[Client]>;
  activeClient$: Observable<Client>;

  constructor(
    private clientService: ClientService,
    private activeNodesService: ActiveNodesService,
    private tokenStorageService: TokenStorageService) { }

  ngOnInit() {
    const user = typeof this.tokenStorageService.getUser() === 'object' ?
    this.tokenStorageService.getUser() : JSON.parse(this.tokenStorageService.getUser());

    if (user.user && user.user.role === 'ADMIN') {
      this.clients$ = this.clientService.getClients();
    }
  }

  changeClient(client) {
    this.activeNodesService.changeClient(client);
  }
}
