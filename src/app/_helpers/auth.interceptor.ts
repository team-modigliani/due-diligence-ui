import { HTTP_INTERCEPTORS, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { TokenStorageService } from '../_services/token-storage.service';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AuthService } from '../_services/auth.service';

const TOKEN_HEADER_KEY = 'authorization';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private token: TokenStorageService, private authService: AuthService, private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authReq = req;
    let token = 'unauthorized';

    if (this.token) {
      token = this.token.getToken();
    }

    authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, `${token}`) });

    return next.handle(authReq).pipe(
      map((event: HttpEvent<any>) => {

        if (event instanceof HttpResponse) {
          if (event.headers.get(TOKEN_HEADER_KEY)) {
            this.token.saveToken(event.headers.get(TOKEN_HEADER_KEY));
          }

          if (event.body.errors && event.body.errors[0].extensions.exception.name === 'AuthorizationError') {
            this.authService.logout();
            this.router.navigateByUrl('/login');
          }
        }

        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        let data = {};
        data = {
            domain: '', //error.domain,
            message: error.message,
            reason: '' //error.reason
        };

        this.authService.logout();
        return throwError(error);
      })
    );
  }
}

export const authInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
];
