import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';
import { asyncScheduler as _async} from 'rxjs';
import { Observable, of, interval } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { cold, getTestScheduler } from 'jasmine-marbles';

import { IntelligenceListComponent } from './intelligence-list.component';
import { ActiveNodesService } from '../../_services/active-nodes.service';
import { SectionService } from '../../_services/section.service';

class MockActiveNodesService {
  private client = new BehaviorSubject({clientId: 'cli1', name: 'Client'});
  currentClient = this.client.asObservable();

  private section = new BehaviorSubject({sectionId: 'sec1', name: 'Section'});
  currentSection = this.section.asObservable();

  private investigation = new BehaviorSubject({investigationId: 'inv1', name: 'Investigation'});
  currentInvestigation = this.investigation.asObservable();

  private intelligence = new BehaviorSubject({investigationId: 'int1', name: 'Intelligence'});
  currentIntelligence = this.intelligence.asObservable();

  changeSections() {}
  changeReport() {}
}

describe('IntelligenceListComponent', () => {
  let component: IntelligenceListComponent;
  let fixture: ComponentFixture<IntelligenceListComponent>;
  let controller: ApolloTestingController;
  let sectionService: any;
  let sectionServiceSpy: jasmine.Spy;
  let element: HTMLElement;

  beforeAll(() => {
    console.log('intelligence-list test');
  });

  beforeEach(async(() => {
    sectionService = jasmine.createSpyObj('SectionService', ['getSectionIntelligence']);
    sectionServiceSpy = sectionService.getSectionIntelligence;

    TestBed.configureTestingModule({
      declarations: [ IntelligenceListComponent ],
      imports: [ ApolloTestingModule ],
      providers: [
        IntelligenceListComponent,
        { provide: ActiveNodesService, useClass: MockActiveNodesService},
        { provide: SectionService, useValue: sectionService }
      ]
    })
    .compileComponents();

    component = TestBed.inject(IntelligenceListComponent);
    controller = TestBed.inject(ApolloTestingController);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntelligenceListComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should display a list of intelligence nodes', () => {
    const q$ = cold('---x|', {x: {
      sectionId: 'sec1',
      intelligence: {
        intelligenceId: 'int1',
        name: 'Executive Summary Intelligence',
        nodes: [{
          __typename: 'Person',
          nodeId: 'per3',
          name: 'Benjamin Franklin'
        }],
        documents: [],
        __typename: 'Intelligence'
      },
      __typename: 'Section'
    }});

    sectionServiceSpy.and.returnValue( q$ );
    fixture.detectChanges();

    element = fixture.nativeElement.querySelector('.card-header');
    expect(element).toBeNull();

    getTestScheduler().flush();
    fixture.detectChanges();

    element = fixture.nativeElement.querySelector('.bg-person');
    expect(element).not.toBeNull();
  });

  it('Should display different nodes with different styles', () => {
    const q$ = cold('---x|', {x: {
      sectionId: 'sec1',
      intelligence: {
        intelligenceId: 'int1',
        name: 'Executive Summary Intelligence',
        nodes: [
          { __typename: 'Person', nodeId: 'n1', name: '' },
          { __typename: 'Event', nodeId: 'n2', name: '' },
          { __typename: 'Object', nodeId: 'n3', name: '' },
          { __typename: 'Area', nodeId: 'n4', name: '' },
          { __typename: 'Address', nodeId: 'n5', name: '' }
        ],
        documents: [],
        __typename: 'Intelligence'
      },
      __typename: 'Section'
    }});

    sectionServiceSpy.and.returnValue( q$ );
    fixture.detectChanges();

    element = fixture.nativeElement.querySelector('.card-header');
    expect(element).toBeNull();

    getTestScheduler().flush();
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('.bg-person')).not.toBeNull();
    expect(fixture.nativeElement.querySelector('.bg-object')).not.toBeNull();
    expect(fixture.nativeElement.querySelector('.bg-area')).not.toBeNull();
    expect(fixture.nativeElement.querySelector('.bg-address')).not.toBeNull();
    expect(fixture.nativeElement.querySelector('.bg-event')).not.toBeNull();
  });

  it('Should display a message when there is no intelligence', () => {
    const q$ = cold('---x|', {x: {
      sectionId: 'sec1',
      intelligence: {},
      __typename: 'Section'
    }});

    sectionServiceSpy.and.returnValue( q$ );
    fixture.detectChanges();

    element = fixture.nativeElement.querySelector('.card-header');
    expect(element).toBeNull();

    getTestScheduler().flush();
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('p').textContent).toContain('no additional intelligence');
  });

  it('Should display a message when the section is not found', () => {
    const q$ = cold('---x|', {x: { } });

    sectionServiceSpy.and.returnValue( q$ );
    fixture.detectChanges();

    element = fixture.nativeElement.querySelector('.card-header');
    expect(element).toBeNull();

    getTestScheduler().flush();
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('p').textContent).toContain('no additional intelligence');
  });
});
