import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { SectionService } from '../../_services/section.service';
import { Section } from '../../../generated/graphql';
import { ActiveNodesService } from '../../_services/active-nodes.service';

@Component({
  selector: 'app-intelligence-list',
  templateUrl: './intelligence-list.component.html',
  styleUrls: ['./intelligence-list.component.scss']
})
export class IntelligenceListComponent implements OnInit {
  intelligence$: Observable<any>;

  @Input()
  sectionId: any;

  constructor(
    private sectionService: SectionService,
    private activeNodesService: ActiveNodesService) { }

  ngOnInit(): void {
    this.intelligence$ = this.sectionService.getSectionIntelligence(this.sectionId);

    this.activeNodesService.currentSection.subscribe(section => {
      this.intelligence$ = this.sectionService.getSectionIntelligence(section.sectionId);
    });
  }
}
