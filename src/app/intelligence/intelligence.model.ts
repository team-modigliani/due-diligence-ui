export class Intelligence {
  id: string;
  name: string;
  content: string;
  people: [any];
  objects: [any];
  locations: [any];
  events: [any];
  documents: [any];
  constructor(id: string, name: string, content?: string, people?: [any], objects?: [any], locations?: [any], events?: [any], documents?: [any]) {
      this.id = id;
      this.name = name;
      this.content = content;
      this.people = people;
      this.objects = objects;
      this.locations = locations;
      this.events = events;
      this.documents = documents;
  }
}
