import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportDetailComponent } from './report/report-detail/report-detail.component';
import { LoginComponent } from './login/login.component';
import { LoggedInGuard } from './logged-in.guard';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { SiteLayoutComponent } from './_layout/site-layout/site-layout.component';
import { ClientLayoutComponent } from './_layout/client-layout/client-layout.component';
import { ReportLayoutComponent } from './_layout/report-layout/report-layout.component';
import { NodeLayoutComponent } from './_layout/node-layout/node-layout.component';
import { ClientReportListComponent } from './client/client-report-list/client-report-list.component';
import { NodeDetailComponent } from './objects/node/node-detail/node-detail.component';

const routes: Routes = [
  {
    path: '',
    component: SiteLayoutComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'unauthorized', component: UnauthorizedComponent }
    ]
  },
  {
    path: 'client',
    canActivate: [LoggedInGuard],
    component: ClientLayoutComponent,
    children: [
      { path: ':id', canActivate: [LoggedInGuard], component: ClientReportListComponent }
    ]
  },
  {
    path: 'report',
    canActivate: [LoggedInGuard],
    component: ReportLayoutComponent,
    children: [
      { path: ':id', canActivate: [LoggedInGuard], component: ReportDetailComponent },
    ]
  },
  {
    path: 'node',
    canActivate: [LoggedInGuard],
    component: NodeLayoutComponent,
    children: [
      { path: ':id', canActivate: [LoggedInGuard], component: NodeDetailComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
