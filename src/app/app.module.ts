import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { MessageModule } from './message/message.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './login/login.component';
import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { PersonDetailComponent } from './objects/people/person/person-detail/person-detail.component';
import { ClientListComponent } from './client/client-list/client-list.component';
import { ReportMenuComponent } from './report/report-menu/report-menu.component';
import { IntelligenceListComponent } from './intelligence/intelligence-list/intelligence-list.component';
import { faUser, faFile, faGlobe, faCircleNotch, faCalendar, faAddressBook, faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { SiteLayoutComponent } from './_layout/site-layout/site-layout.component';
import { SiteHeaderComponent } from './_layout/site-header/site-header.component';
import { ClientLayoutComponent } from './_layout/client-layout/client-layout.component';
import { ClientHeaderComponent } from './_layout/client-header/client-header.component';
import { SiteFooterComponent } from './_layout/site-footer/site-footer.component';
import { ReportLayoutComponent } from './_layout/report-layout/report-layout.component';
import { NodeLayoutComponent } from './_layout/node-layout/node-layout.component';
import {APOLLO_OPTIONS} from 'apollo-angular';
import {HttpLink} from 'apollo-angular/http';
import {InMemoryCache} from '@apollo/client/core';
import { environment } from './../environments/environment';
import { ClientReportListComponent } from './client/client-report-list/client-report-list.component';
import { ReportOverviewComponent } from './report/report-overview/report-overview.component';
import { NodeDetailComponent } from './objects/node/node-detail/node-detail.component';
import { NodeConnectionsComponent } from './objects/node/node-connections/node-connections.component';
import { NodeIconComponent } from './objects/node/node-icon/node-icon.component';
import { NodeReferencesComponent } from './objects/node/node-references/node-references.component';
import { AreaDetailComponent } from './objects/locations/area-detail/area-detail.component';
import { AddressDetailComponent } from './objects/locations/address-detail/address-detail.component';
import { NodeMatchedReferencesComponent } from './objects/node/node-matched-references/node-matched-references.component';
import { ObjectDetailComponent } from './objects/object/object-detail/object-detail.component';
import { EventDetailComponent } from './objects/event/event-detail/event-detail.component';
import { ReportDetailComponent } from './report/report-detail/report-detail.component';
import { ReferenceListComponent } from './reference/reference-list/reference-list.component';
import { CommentListComponent } from './comment/comment-list/comment-list.component';
import { CreateCommentComponent } from './comment/create-comment/create-comment.component';
import { CommentComponent } from './comment/comment/comment.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UnauthorizedComponent,
    PersonDetailComponent,
    ClientListComponent,
    ReportMenuComponent,
    IntelligenceListComponent,
    SiteLayoutComponent,
    SiteHeaderComponent,
    ClientLayoutComponent,
    ClientHeaderComponent,
    SiteFooterComponent,
    ReportLayoutComponent,
    NodeLayoutComponent,
    ClientReportListComponent,
    ReportOverviewComponent,
    NodeDetailComponent,
    NodeConnectionsComponent,
    NodeIconComponent,
    NodeReferencesComponent,
    AreaDetailComponent,
    AddressDetailComponent,
    NodeMatchedReferencesComponent,
    ObjectDetailComponent,
    EventDetailComponent,
    ReportDetailComponent,
    ReferenceListComponent,
    CommentListComponent,
    CreateCommentComponent,
    CommentComponent
  ],
  imports: [
    FontAwesomeModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MessageModule,
    NgbModule,
  ],
  providers: [
    NgbActiveModal,
    authInterceptorProviders,
    {
      provide: APOLLO_OPTIONS,
      useFactory: (httpLink: HttpLink) => {
        return {
          cache: new InMemoryCache(),
          link: httpLink.create({
            uri: environment.GRAPHQL_SERVER
          }),
        };
      },
      deps: [HttpLink],
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    // Add an icon to the library for convenient access in other components
    library.addIcons(faUser, faFile, faGlobe, faCircleNotch, faCalendar, faAddressBook, faMapMarkerAlt);
  }
}