import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule,  ReactiveFormsModule } from '@angular/forms';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';
import { MessageComponent } from './message/message.component';
import { ReportListComponent } from './report/report-list/report-list.component';
import { SiteFooterComponent } from './_layout/site-footer/site-footer.component';
import { IntelligenceListComponent } from './intelligence/intelligence-list/intelligence-list.component';
import { NodeMatchedReferencesComponent } from './objects/node/node-matched-references/node-matched-references.component';
import { AppComponent } from './app.component';
import { TokenStorageService } from './_services/token-storage.service';

class MockTokenStorageService {
  getUser() {
    return {
      user: {
        role: 'ADMIN'
      }
    }
  }
}

describe('AppComponent', () => {
  let controller: ApolloTestingController;

  beforeAll(() => {
    console.log('app.component test');
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        RouterTestingModule,
        ReactiveFormsModule,
        ApolloTestingModule
      ],
      declarations: [
        AppComponent,
        MessageComponent,
        ReportListComponent,
        SiteFooterComponent,
      ],
      providers: [AppComponent, { provide: TokenStorageService, useClass: MockTokenStorageService } ],

    }).compileComponents();

    controller = TestBed.inject(ApolloTestingController);
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
